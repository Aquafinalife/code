<?php echo $this->load->view('common/header'); ?>
<body class="category-page option6">
<?php 
    $this->load->library('Mobile_Detect');
    $detect = new Mobile_Detect();
    if($detect->isMobile() && !$detect->isTablet()){
        $isMobile = true;
    } else {
        $isMobile = false;
    }
    $config = get_cache('configurations_' . get_language());
?>
<?php echo $this->load->view('common/top'); ?>


<div class="columns-container">
    <div class="container" id="columns">
        
        
    <div class="row">
        <div class="col-sm-12">
            <?php if (isset($main_content)) {echo $main_content;} ?>
        </div>
    </div>
  
    </div>
</div>


<?php echo $this->load->view('common/footer'); ?>

</body>
</html>