<?php $advs = modules::run('advs/get_advs_slide_show',array('type'=>7)); ?>
<?php if(!empty($advs)) { ?>
<div class="homeads2">
    <?php foreach($advs as $key => $value){
        $url_path = ($value->url_path == "" || $value->url_path == "#") ? "" : $value->url_path;
        $title = ($value->title == NULL || $value->title == '') ? '' : $value->title;
        $image = $value->image_name;
        $image_url = base_url() . ADVS_IMAGE_URL . $image;
    ?>
    <a <?php if($url_path <> ''){ ?>href="<?php echo $url_path; ?>"<?php } ?> title="<?php echo $title; ?>" >
        <img title="<?php echo $title; ?>" alt="" src="<?php echo $image_url; ?>" border="0" >
    </a>
    <?php } ?>
</div>
<?php } ?>