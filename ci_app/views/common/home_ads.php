<?php $advs6 = modules::run('advs/get_advs_slide_show',array('type'=>9, 'onehit'=>TRUE));
$advs7 = modules::run('advs/get_advs_slide_show',array('type'=>10, 'onehit'=>TRUE));
$advs8 = modules::run('advs/get_advs_slide_show',array('type'=>11, 'onehit'=>TRUE));
$advs9 = modules::run('advs/get_advs_slide_show',array('type'=>12, 'onehit'=>TRUE));
?>

<div class="homeads0">
    <div class="col-sm-4">
        <?php if(!empty($advs6)) {
            $url_path = ($advs6->url_path == "" || $advs6->url_path == "#") ? "" : $advs6->url_path;
            $title = ($advs6->title == NULL || $advs6->title == '') ? '' : $advs6->title;
            $image = $advs6->image_name;
            $image_url = base_url() . ADVS_IMAGE_URL . $image;
        ?>
        <a <?php if($url_path <> ''){ ?>href="<?php echo $url_path; ?>"<?php } ?> title="<?php echo $title; ?>" >
            <img title="<?php echo $title; ?>" alt="" src="<?php echo $image_url; ?>" border="0" >
        </a>
        <?php } ?>
    </div>
    <div class="col-sm-4">

        <?php if(!empty($advs7)) {
            $url_path = ($advs7->url_path == "" || $advs7->url_path == "#") ? "" : $advs7->url_path;
            $title = ($advs7->title == NULL || $advs7->title == '') ? '' : $advs7->title;
            $image = $advs7->image_name;
            $image_url = base_url() . ADVS_IMAGE_URL . $image;
        ?>
        <a <?php if($url_path <> ''){ ?>href="<?php echo $url_path; ?>"<?php } ?> title="<?php echo $title; ?>" >
            <img title="<?php echo $title; ?>" alt="" src="<?php echo $image_url; ?>" border="0" >
        </a>
        <?php } ?>

        <?php if(!empty($advs8)) {
            $url_path = ($advs8->url_path == "" || $advs8->url_path == "#") ? "" : $advs8->url_path;
            $title = ($advs8->title == NULL || $advs8->title == '') ? '' : $advs8->title;
            $image = $advs8->image_name;
            $image_url = base_url() . ADVS_IMAGE_URL . $image;
        ?>
        <a <?php if($url_path <> ''){ ?>href="<?php echo $url_path; ?>"<?php } ?> title="<?php echo $title; ?>" >
            <img title="<?php echo $title; ?>" alt="" src="<?php echo $image_url; ?>" border="0" >
        </a>
        <?php } ?>

    </div>
    <div class="col-sm-4">
        <?php if(!empty($advs9)) {
            $url_path = ($advs9->url_path == "" || $advs9->url_path == "#") ? "" : $advs9->url_path;
            $title = ($advs9->title == NULL || $advs9->title == '') ? '' : $advs9->title;
            $image = $advs9->image_name;
            $image_url = base_url() . ADVS_IMAGE_URL . $image;
        ?>
        <a <?php if($url_path <> ''){ ?>href="<?php echo $url_path; ?>"<?php } ?> title="<?php echo $title; ?>" >
            <img title="<?php echo $title; ?>" alt="" src="<?php echo $image_url; ?>" border="0" >
        </a>
        <?php } ?>
    </div>
</div>
