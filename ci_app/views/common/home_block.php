<?php $results = modules::run('products/get_products_home'); ?>
<?php if (!empty($results)) { ?>
    <?php
    
    foreach ($results as $k => $result) {
        if (SLUG_ACTIVE == 0) {
            $more_link = get_base_url() . url_title(trim($result['category']), 'dash', TRUE) . '-p' . $result['id'];
        } else {
            $more_link = get_base_url() . $result['category_slug'];
        }
        $image_cat = is_null($result['thumbnail']) ? base_url() . 'images/cart.png' : base_url() .  $result['thumbnail'];
        //bỏ icon
        $image_icon = is_null($result['avatar']) ? base_url() . 'images/cart.png' : base_url() .  $result['avatar'];
        $link_banner = is_null($result['link']) ? '#' : base_url() .  $result['link'];
        //$alt = is_null($result['alt']) ? $result['category'] :  $result['alt'];
        if(!empty($result['alt'])){           
            $alt =$result['alt'];
        }else{
            $alt = $result['category'];
        }
        if(!empty($result['content'])){           
            $content =$result['content'];
        }else{
            $content = $result['category'];
        }
        if(!empty($result['css'])){           
            $css = $result['css'];
        }else{
            $css ='fashion';
        }
        ?>
        <?php if (!empty($result['products'])) { ?>
            <div class="category-featured <?php echo $css;?>">
                <nav class="navbar nav-menu show-brand">
                    <div class="container">
                      <!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-brand">
    <a href="<?php echo $more_link; ?>">
        <img alt="<?php echo $result['category'];?>" title="<?php echo $result['category'];?>" src="<?php echo $image_icon;?>" />
        <?php echo $result['category']; ?>
        
    </a>
</div>
                        
                        <span class="toggle-menu"></span>
                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse">           
                        <ul class="nav navbar-nav">
                          <li class="active"><a data-toggle="tab" href="#tab-4">Sản phẩm mới nhất</a></li>
                         
                        </ul>
                      </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                    <div id="elevator-1" class="floor-elevator">
                          <a href="#" class="btn-elevator up disabled fa fa-angle-up"></a>
                          <a href="#elevator-2" class="btn-elevator down fa fa-angle-down"></a>
                    </div>
                </nav>
                <div class="product-featured clearfix">
                    <div class="row">
                        <div class="col-sm-2 sub-category-wapper">
                            
<?php $list_child_cats = $this->products_categories_model->get_categories(array('parent_id'=>$result['id']));?>
<ul class="sub-category-list">
<?php
if(isset($list_child_cats)){
foreach ($list_child_cats as $k => $list_child_cat) {
if (SLUG_ACTIVE == 0) {
$link_cat = get_base_url() . url_title(trim($list_child_cat->category), 'dash', TRUE) . '-p' . $list_child_cat->id;
} else {
$link_cat = get_base_url() . $list_child_cat->slug;
}
$highl = $list_child_cat->highlight;
if($highl ==1){
    $highl = 'highlight';
}  else {
    $highl= '';
}
?>
<li><a class ="<?php echo $highl;?>" href="<?php echo $link_cat; ?>"><?php echo $list_child_cat->category ;?></a></li>
<?php }}?>
                              

</ul>
                        </div>
                        <div class="col-sm-10 col-right-tab">
                            <div class="product-featured-tab-content">
                                <div class="tab-container">
                                    <div class="tab-panel active" id="tab-4">
                                        <div class="box-left">
                                            <div class="banner-img">
                                                <a href="<?php echo $link_banner; ?>">
                                                    <img class="banner-img-1" src="<?php echo $image_cat; ?>" alt="<?php echo $alt;?>" title="<?php echo $alt;?>">
                                                    
                                                    <div class="event-info" >
                                                    <span class="main-title" ><?php echo $alt;?></span>
                                                    <p class="sub-title" ><?php echo $content;?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="box-right">
                                            <ul class="product-list row">
                                                <?php
                                                foreach ($result['products'] as $key => $value) {
                                                    if (SLUG_ACTIVE == 0) {
                                                        $uri = get_base_url() . url_title(trim($value->product_name), 'dash', TRUE) . '-ps' . $value->id;
                                                    } else {
                                                        $uri = get_base_url() . $value->slug;
                                                    }
                                                    $image = is_null($value->image_name) ? base_url() . 'images/no-image.png' : base_url() . 'images/products/thumbnails/' . $value->image_name;
                                                    $value_name = limit_text($value->product_name, 50);
                                                    $value_category = limit_text($value->category, 100);
                                                    $price = $value->price > 0 ? get_price_in_vnd($value->price) . ' ₫' : get_price_in_vnd($value->price);
                                                    if(!empty($value->price_old)){
                                                    $price_old = $value->price_old > 0 ? get_price_in_vnd($value->price_old) . ' ₫' : 0;
                                                    $price_count = $value->price_old - $value->price;
                                                  
                                                    if($price_count < 0 ){
                                                        
                                                        $price_count = $price_count * -1;
                                                        $am = true;
                                                    }
                                                    $price_discount = $price_count > 0 ? get_price_in_vnd($price_count) . ' ₫' : 0;
                                                    $saleoff = ($price_count / $value->price_old) * 100;
                                                    $saleoff = round($saleoff, 0);
                                                    }
                                                    ?>
                                                    <li class="col-sm-4">
                                                        <div class="right-block">
                                                            <h5 class="product-name"><a title="<?php echo $value->product_name; ?>" href="<?php echo $uri; ?>"><?php echo $value_name; ?></a></h5>
                                                            <div class="content_price">
                                                                <div class="price-new-old">
                                                                    <span class="price product-price"><?php echo $price; ?></span>
                                                                    <?php if (!empty($value->price_old) && $value->price_old <> 0) { ?>
                                                                    <span class="price old-price"><?php echo $price_old;?></span>
                                                                    <?php } ?>
                                                                </div>
                                                               
                                                                <?php if (!empty($value->price_old) && $value->price_old <> 0) { ?>
                                                                    <div class="field-sale">
                                                                        <?php if($am == false){?>
                                                                        <span class="field-sale-1">Giảm </span><br><span class="discount"><?php echo '-' . $saleoff . '%'; ?></span>  
                                                                        <?php }else {?>
                                                                         <span class="field-sale-1">Tăng</span><br><span class="discount"><?php echo '+' . $saleoff . '%'; ?></span>                                                             
                                                                        <?php }?>
                                                                    </div>
                                                                <?php } ?>
                                                               

                                                            </div>
                                                        </div>
                                                        <div class="left-block">
                                                            <a href="<?php echo $uri; ?>">
                                                                <img class="img-responsive" alt="<?php echo $value->product_name; ?>" title="<?php echo $value->product_name; ?>" src="<?php echo $image; ?>" />
                                                            </a>
<!--                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart" href="#"></a>
                                                                <a title="Add to compare" class="compare" href="#"></a>
                                                                <a title="Quick view" class="search" href="#"></a>
                                                            </div>-->
                                                            <div class="add-to-cart">
                                                                <a title="Add to Cart" onclick="fast_addtocart('<?php echo $value->id;?>');">Mua ngay</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php  $am = false;} ?>      

                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<div class="clearfix"></div>
        <?php } ?>
    <?php } ?>

<?php } ?>