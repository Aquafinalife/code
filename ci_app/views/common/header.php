<!DOCTYPE html>
<html>
<head>
<title><?php if(!empty($title)){echo htmlspecialchars($title);}else{echo __('IP_DEFAULT_TITLE');} ?></title>
<meta http-equiv="Content-language" content="vi" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="title" content="<?php if (!empty($title)){echo htmlspecialchars($title);}else{echo __('IP_DEFAULT_TITLE');} ?>" />
<meta name="keywords" content="<?php if (!empty($keywords)){echo htmlspecialchars($keywords);}else{echo __('IP_DEFAULT_KEYWORDS');} ?>" />
<meta name="description" content="<?php if (!empty($description)){echo htmlspecialchars($description);}else{echo __('IP_DEFAULT_DESCRIPTION');} ?>" />

<meta name="format-detection" content="telephone=no">
<?php 
$config = get_cache('configurations_' .  get_language());
?>
<?php 
if(!empty($config)){
    echo (isset($config['webmaster_tracker']))?$config['webmaster_tracker']:'';
    $favicon = !empty($config['favicon'])?base_url().UPLOAD_URL_FAVICON.$config['favicon']:base_url().'images/favicon.png';
}
?>

<link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon">
<link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">

<!-- style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/lib/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/lib/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/bootstrap-theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/lib/select2/css/select2.min.css" />
    <!--<link rel="stylesheet" type="text/css" href="<?php // echo base_url(); ?>frontend/lib/jquery.bxslider/jquery.bxslider.css" />-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/lib/owl.carousel/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/lib/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/style.css" />

    

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/responsive.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontend/css/option6.css" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
