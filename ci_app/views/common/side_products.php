<?php $data = modules::run('products/get_side_products'); ?>
<?php if(!empty($data)) { ?>
<div class="sidebox sideproducts">
    <h3><?php echo __('IP_products_top'); ?></h3>
    <div class="sideproducts_inner">
    <?php foreach($data as $key => $value){
        if(SLUG_ACTIVE==0){
            $uri = get_base_url() . url_title(trim($value->product_name), 'dash', TRUE) . '-ps' . $value->id;
        }else{
            $uri = get_base_url() . $value->slug;
        }
        $image = is_null($value->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/thumbnails/'.$value->image_name;
        $product_name = limit_text($value->product_name, 50);
//        $price = $value->price != 0 ? get_price_in_vnd($value->price) . ' ₫' : get_price_in_vnd($value->price);           
    ?>
        <div class="sideproducts_item">
            <div class="row">
                <div class="col-sm-12 sideproducts_item_image">
                    <a href="<?php echo $uri; ?>" title="<?php echo $value->product_name; ?>">
                        <img alt="" src="<?php echo $image; ?>" title="<?php echo $value->product_name; ?>" >
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 sideproducts_item_text">
                    <h5><a href="<?php echo $uri; ?>" title="<?php echo $value->product_name; ?>"><?php echo $product_name; ?></a></h5>
                    <!--<strong><?php // echo $price; ?></strong>-->
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<?php } ?>