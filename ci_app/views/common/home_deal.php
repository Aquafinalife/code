<?php $data = modules::run('products/get_list_products_home',array('onehit'=>TRUE,'saleoff'=>TRUE)); ?>
<?php if(!empty($data)){ 
    if(SLUG_ACTIVE==0){
        $uri = get_base_url() . url_title(trim($data->product_name), 'dash', TRUE) . '-ps' . $data->id;
    }else{
        $uri = get_base_url() . $data->slug;
    }
    $image = is_null($data->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/thumbnails/'.$data->image_name;
    $data_name = limit_text($data->product_name, 100);
    $price = $data->price > 0 ? get_price_in_vnd($data->price) . ' ₫' : get_price_in_vnd($data->price);   
    $price_old = $data->price_old > 0 ? get_price_in_vnd($data->price_old) . ' ₫' : 0;
    $price_count = $data->price_old - $data->price;
    $price_discount = $price_count > 0 ? get_price_in_vnd($price_count) . ' ₫' : 0;
    $saleoff = $data->price_old > 0 ? ($price_count/$data->price_old)*100 : 0;
    $saleoff = round($saleoff,0);
?>
<div class="home_deal">
    <h4 class="timer">Giá tốt hôm nay</h4>
    <a title="<?php echo $data->product_name; ?>" href="<?php echo $uri; ?>">
        <?php if(!empty($saleoff) && $saleoff > 0){ ?>
        <p class="saleoff"><span><?php echo '-' . $saleoff . '%'; ?></span></p>
        <?php } ?>
        <img alt="" title="<?php echo $data->product_name; ?>" src="<?php echo $image; ?>" />
        <p class="price"><?php echo $price; ?></p>
    </a>
    <h3><a title="<?php echo $data->product_name; ?>" href="<?php echo $uri; ?>"><?php echo $data_name; ?></a></h3>
    <div class="row">
        <div class="col-sm-12">
            <p><strong>Giá gốc: </strong><span class="price_old"><?php echo $price_old; ?></span></p>
            <p><strong>Tiết kiệm: </strong><span class="price_discount"><?php echo $price_discount; ?></span></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a class="buynow" title="<?php echo $data->product_name; ?>" href="<?php echo $uri; ?>">Mua ngay</a>
        </div>
    </div>
</div>

<!--<div class="link-more">
    <a alt="xem nhiều hơn nữa" title="xem nhiều hơn nữa" href="#">Xem nhiều giá tốt hơn nữa <i class="fa fa-arrow-right"></i></a>
</div>-->
<?php } ?>