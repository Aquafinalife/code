<?php

class Common extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        
    }

    function ajax_get_dn_bbh($options = array()) {

        $html = '';

        $data = '<select name="cn_bbh" class="">
                        <option value="">--Chọn chi nhánh/phòng ban BH--</option>
                    </select>';
        $res = array(
            'cn_bbh' => $data,
            'goi_bh'=>'<select name="goi_bh" class="">
                        <option value="">--Chọn gói BH--</option>
                    </select>',
        );

        $parent_id = $_POST['parent_id'];
        $goi_bh = isset($_POST['goi_bh']) ? $_POST['goi_bh'] : '';
        if ($parent_id >= 0 && $parent_id != '') {
            //$options['parent_id'] = $_POST['parent_id'];
            // Lấy chi nhánh phòng ban bbh
            $data_get = array('parent_id' => $parent_id);
            $this->load->model('products/products_trademark_model');
            $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);

            if (!empty($dn_bbh)) {
                foreach ($dn_bbh as $index) {
                    $name = $index->name;
                    $parent_id = $index->parent_id;
                    $id = $index->id;
                    $html .= '<option value="' . $id . '">' . $name . '</option>';
                }
                $data = '<select name="cn_bbh" class="">
                        <option value="">--Chọn chi nhánh/phòng ban BH--</option>
                        ' . $html . '
                    </select>';
                $res = array(
                    'cn_bbh' => $data,
                );
            }
            if ($goi_bh > 0 && $goi_bh != '') {
                // Lấy các gói BH của cn bbh
                $this->load->model('products/products_size_model');
                $data_get = array(
                    'dn_bbh' => $parent_id,
                );
                $a_goi_bh = $this->products_size_model->get_products_size($data_get);
                
                if (!empty($a_goi_bh)) {
                    $html ='';
                    foreach ($a_goi_bh as $index) {
                        $name = $index->name;
                        $id = $index->id;
                        $html .= '<option value="' . $id . '">' . $name . '</option>';
                    }
                    $data = '<select name="goi_bh" class="">
                        <option value="">--Chọn gói BH--</option>
                        ' . $html . '
                    </select>';
                    $res['goi_bh']= $data;
                }
            }
        }
        //end
        echo json_encode($res);
        exit();
    }

    //end
    function ajax_get_customers_by_options($options = array()) {

        $html = '';

        $phanloai_kh = $_POST['type'];
        if ($phanloai_kh != '') {
            $options['phanloai_kh'] = $phanloai_kh;
        }
        $parent_id = $_POST['parent_id'];
        if ($parent_id >= 0 && $parent_id != '') {
            $options['parent_id'] = $_POST['parent_id'];
        }
        //end
        $selectbox_name = $_POST['selectbox_name'];
        $selectbox_id = $_POST['selectbox_id'];
        $select_df = isset($_POST['select_df']) ? $_POST['select_df'] : 'Tất cả doanh nghiệp BH';

        if ($phanloai_kh == DOANHNGHIEP) {
            $this->load->model('customers/customers_model');
            $customers = $this->customers_model->get_customers($options);

            if (!empty($customers)) {
                foreach ($customers as $index) {
                    $id = $index->id;
                    $congty_canhan = $index->congty_canhan;
                    $html .= '<option value="' . $id . '">' . $congty_canhan . '</option>';
                }
            }
            $data = '<select type="' . DOANHNGHIEP . '" selectbox_name="' . $selectbox_name . '" name="' . $selectbox_name . '" class="" id="' . $selectbox_id . '">
                        <option value="">' . $select_df . '</option>
                        ' . $html . '
                    </select>';
            echo $data;
        } else {
            echo 'FALSE';
        }
        exit();
    }

    //end
    function ajax_get_customers_by_options_bc($options = array()) {

        $html = '';

        $phanloai_kh = $_POST['type'];

        if ($phanloai_kh != '') {
            $options['phanloai_kh'] = $phanloai_kh;
        }
        $parent_id = $_POST['parent_id'];
        if ($parent_id >= 0 && $parent_id != '') {
            $options['parent_id'] = $_POST['parent_id'];
        }
        //end
        $selectbox_name = $_POST['selectbox_name'];
        $selectbox_id = $_POST['selectbox_id'];
        $select_df = isset($_POST['select_df']) ? $_POST['select_df'] : '--Chọn doanh nghiệp mua BH--';
        // Lấy khách hàng
        $this->load->model('customers/customers_model');
        $customers = $this->customers_model->get_customers($options);
        //end
        if ($phanloai_kh == CANHAN) {
            // Lấy cá nhân được mua BH
            $ca_nhan_dbh = $this->customers_model->get_canhan_dbh();
            if (!empty($ca_nhan_dbh)) {
                $a_mst_cmt = array();
                foreach ($ca_nhan_dbh as $index) {
                    $id = $index->id;
                    $mst_cmt = $index->mst_cmt;
                    $congty_canhan = $index->congty_canhan;
                    if (!in_array($mst_cmt, $a_mst_cmt)) {
                        $html .= '<option value="' . $mst_cmt . '">' . $mst_cmt . ' -- ' . $congty_canhan . '</option>';
                    }
                    $a_mst_cmt[] = $mst_cmt;
                }
            }
            $data .='<label for="exampleInputName2">Tìm theo CMT/Hộ chiếu</label>
                    <select name="key" class="js-example-disabled-results">
                    <option value="">--Điền Tên hoặc CMT--</option>
                    ' . $html . '
                    </select>';
            echo $data;
            exit();
        } elseif ($phanloai_kh == DOANHNGHIEP) {

            if (!empty($customers)) {
                foreach ($customers as $index) {
                    $id = $index->id;
                    $congty_canhan = $index->congty_canhan;
                    $html .= '<option value="' . $id . '">' . $congty_canhan . '</option>';
                }
            }
            $data = '<select type="' . DOANHNGHIEP . '" selectbox_name="' . $selectbox_name . '" name="' . $selectbox_name . '" class="js-example-disabled-results" id="' . $selectbox_id . '">
                        <option value="">' . $select_df . '</option>
                        ' . $html . '
                    </select>';
            echo $data;
            exit();
        } else {
            echo 'FALSE';
        }
        exit();
    }

}

?>
