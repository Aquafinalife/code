<?php

class Products_Size_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
    }

    function browse() {
        $options = array();

        $options['products_size'] = $this->products_size_model->get_products_size();

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('admin/products_size/list', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Quản lý Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //$this->_get_add_form_data($options);
        $this->_get_form_data($options);
        // Chuan bi du lieu chinh de hien thi
        //$options['name'] = $this->input->post('name');
        $options['header'] = 'Thêm Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_ADD_URL;

        $this->_view_data['main_content'] = $this->load->view('admin/products_size/form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Thêm Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_form_data(&$options = array()) {

        // Lấy thông tin các dn bán BH
        $data_get = array();
        $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
       
        if (!empty($dn_bbh)) {
            $options['a_dn_bbh'] = $dn_bbh;
        }
        //end
    }

    /**
     *
     * @return type 
     */
    private function _get_add_form_data(&$options = array()) {

        $options['name'] = $this->input->post('name');
        $options['header'] = 'Thêm Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_ADD_URL;
    }

    private function _do_add() {

        $this->form_validation->set_rules('name', 'Gói bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('gia_tien', 'Giá tiền', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('TV_TTTBVV_TN', 'Tử vong,TTTBVV do tai nạn', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('TCL_NN', 'Trợ cấp lương ngày nghỉ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tcl_nn_sn', 'Số ngày - Trợ cấp lương ngày nghỉ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('CPYT_TN', 'Chi phí y tế do tai nạn', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('TV_TTTBVV_OB', 'Tử vong,TTTBVV do ốm bệnh', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('DTNT_OB', 'Điều trị nội trú (do ốm bệnh)', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_dbh', 'Điều trị nội trú - Đồng bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tvpn', 'Tiền viện phí ngày', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tvpn_sn', 'Số ngày / Tiền viện phí ngày', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tgpn', 'Tiền giường,phòng / ngày', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tgpn_sn', 'Số ngày / Tiền giường phòng', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_cppt', 'Chi phí phẫu thuật', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tk_nv', 'Quyền lợi khám trước khi nhập viện', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_sk_nv', 'Quyền lợi khám sau khi nhập viện', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_cpyt_tn', 'Chi phí y tá tại nhà', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_cpyt_tn_sn', 'Số ngày / Chi phí y tá tại nhà', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tcvp', 'Trợ cấp viện phí', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tcvp_sn', 'Số ngày / Trợ cấp viện phí', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_tcmt', 'Trợ cấp mai táng', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtnt_xct', 'Xe cứu thương', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsqlnt_ktdk', 'Chi phí khám thai định kỳ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsqlnt_st', 'Sinh thường', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsqlnt_sm', 'Sinh mổ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsqlnt_dn', 'Dưỡng nhi', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('DTNGT_OB', 'Điều trị ngoại trú (do ốm bệnh)', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtngt_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtngt_st1lk', 'Số tiền cho 1 lần khám', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtngt_slk', 'Số lần khám', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtngt_nk', 'Nha khoa (trong OP)', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('dtngt_cvr', 'Cạo vôi răng', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('THAISAN_MDL', 'THAI SẢN (mua độc lập)', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsmdl_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsmdl_ktdk', 'Chi phí khám thai định kỳ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsmdl_st', 'Sinh thường', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsmdl_sm', 'Sinh mổ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tsmdl_dn', 'Dưỡng nhi', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('NHAKHOA_MDL', 'Nha khoa (mua độc lập)', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('nkmdl_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('nkmdl_cb', 'Nha khoa cơ bản', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('nkmdl_db', 'Nha khoa đặc biệt', 'trim|required|xss_clean|max_length[255]');
        //
        $this->form_validation->set_rules('dn_bbh', 'Công ty Bảo Hiểm:', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('cn_bbh', 'Chi nhánh/phòng ban', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('moigioi', 'Môi giới', 'trim|required|xss_clean|max_length[255]');
        
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'gia_tien' => my_trim($this->input->post('gia_tien')),
                'status' => STATUS_ACTIVE,
                'creator' => $this->phpsession->get('user_id'),
                'editor' => $this->phpsession->get('user_id'),
                'TV_TTTBVV_TN' => my_trim($this->input->post('TV_TTTBVV_TN')),
                'TCL_NN' => my_trim($this->input->post('TCL_NN')),
                'tcl_nn_sn' => my_trim($this->input->post('tcl_nn_sn')),
                'CPYT_TN' => my_trim($this->input->post('CPYT_TN')),
                'TV_TTTBVV_OB' => my_trim($this->input->post('TV_TTTBVV_OB')),
                'DTNT_OB' => my_trim($this->input->post('DTNT_OB')),
                'dtnt_dbh' => my_trim($this->input->post('dtnt_dbh')),
                'dtnt_tvpn' => my_trim($this->input->post('dtnt_tvpn')),
                'dtnt_tvpn_sn' => my_trim($this->input->post('dtnt_tvpn_sn')),
                'dtnt_tgpn' => my_trim($this->input->post('dtnt_tgpn')),
                'dtnt_tgpn_sn' => my_trim($this->input->post('dtnt_tgpn_sn')),
                'dtnt_cppt' => my_trim($this->input->post('dtnt_cppt')),
                'dtnt_tk_nv' => my_trim($this->input->post('dtnt_tk_nv')),
                'dtnt_sk_nv' => my_trim($this->input->post('dtnt_sk_nv')),
                'dtnt_cpyt_tn' => my_trim($this->input->post('dtnt_cpyt_tn')),
                'dtnt_cpyt_tn_sn' => my_trim($this->input->post('dtnt_cpyt_tn_sn')),
                'dtnt_tcvp' => my_trim($this->input->post('dtnt_tcvp')),
                'dtnt_tcvp_sn' => my_trim($this->input->post('dtnt_tcvp_sn')),
                'dtnt_tcmt' => my_trim($this->input->post('dtnt_tcmt')),
                'dtnt_xct' => my_trim($this->input->post('dtnt_xct')),
                'tsqlnt_ktdk' => my_trim($this->input->post('tsqlnt_ktdk')),
                'tsqlnt_st' => my_trim($this->input->post('tsqlnt_st')),
                'tsqlnt_sm' => my_trim($this->input->post('tsqlnt_sm')),
                'tsqlnt_dn' => my_trim($this->input->post('tsqlnt_dn')),
                'DTNGT_OB' => my_trim($this->input->post('DTNGT_OB')),
                'dtngt_dbh' => my_trim($this->input->post('dtngt_dbh')),
                'dtngt_st1lk' => my_trim($this->input->post('dtngt_st1lk')),
                'dtngt_slk' => my_trim($this->input->post('dtngt_slk')),
                'dtngt_nk' => my_trim($this->input->post('dtngt_nk')),
                'dtngt_cvr' => my_trim($this->input->post('dtngt_cvr')),
                'THAISAN_MDL' => my_trim($this->input->post('THAISAN_MDL')),
                'tsmdl_dbh' => my_trim($this->input->post('tsmdl_dbh')),
                'tsmdl_ktdk' => my_trim($this->input->post('tsmdl_ktdk')),
                'tsmdl_st' => my_trim($this->input->post('tsmdl_st')),
                'tsmdl_sm' => my_trim($this->input->post('tsmdl_sm')),
                'tsmdl_dn' => my_trim($this->input->post('tsmdl_dn')),
                'NHAKHOA_MDL' => my_trim($this->input->post('NHAKHOA_MDL')),
                'nkmdl_dbh' => my_trim($this->input->post('nkmdl_dbh')),
                'nkmdl_cb' => my_trim($this->input->post('nkmdl_cb')),
                'nkmdl_db' => my_trim($this->input->post('nkmdl_db')),
                //
                'dn_bbh' => my_trim($this->input->post('dn_bbh')),
                'cn_bbh' => my_trim($this->input->post('cn_bbh')),
                'moigioi' => my_trim($this->input->post('moigioi')),
            );

            $id = my_trim($this->input->post('id'));
            if ($id > 0) {
                $data['id'] = $id;
                $this->products_size_model->update($data);
            } else {
                $this->products_size_model->insert($data);
            }

            redirect(PRODUCTS_SIZE_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    function edit() {
        $options = array();
        if ($this->is_postback()) {

            if (!$this->input->post('from_list')) {
                //if (!$this->_do_edit()) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //$this->_get_edit_form_data($options);
        $this->_get_form_data($options);
        // Chuan bi du lieu chinh de hien thi
        $id = $this->input->post('id');
        // Get products size
        $size = $this->products_size_model->get_products_size(array('id' => $id));

        if (is_object($size)) {
            $options['size'] = $size;
        }

        //$options['id'] = $id;
        $options['header'] = 'Sửa Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_EDIT_URL;

        $this->_view_data['main_content'] = $this->load->view('admin/products_size/form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Sửa Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_edit_form_data(&$options = array()) {
        $id = $this->input->post('id');
        if ($this->input->post('from_list')) {
            $products_size = $this->products_size_model->get_products_size(array('id' => $id));
            $options['name'] = $products_size->name;
            $options['gh_tv_tn'] = $products_size->gh_tv_tn;
            $options['tt_bp'] = $products_size->tt_bp;
        } else {
            $options['name'] = $this->input->post('name');
            $options['gh_tv_tn'] = $this->input->post('gh_tv_tn');
            $options['tt_bp'] = $this->input->post('tt_bp');
        }
        $options['id'] = $id;
        $options['header'] = 'Sửa Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_EDIT_URL;
    }

    private function _do_edit() {
        $this->form_validation->set_rules('name', 'Gói bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run()) {
            $data = array(
                'id' => $this->input->post('id'),
                'name' => my_trim($this->input->post('name', TRUE)),
                'gh_tv_tn' => my_trim($this->input->post('gh_tv_tn', TRUE)),
                'tt_bp' => my_trim($this->input->post('tt_bp', TRUE)),
                'editor' => $this->phpsession->get('user_id'),
            );
            $this->products_size_model->update($data);
            redirect(PRODUCTS_SIZE_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('size' => $id));
            if (empty($check1)) {
                $this->products_size_model->delete($id);
            }
            redirect(PRODUCTS_SIZE_ADMIN_BASE_URL);
        }
    }

}
