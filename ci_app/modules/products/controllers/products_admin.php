<?php

class Products_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login', $this->router->fetch_module());
        $this->_layout = 'admin_ui/layout/main';
        $this->_view_data['url'] = PRODUCTS_ADMIN_BASE_URL;
//        $this->output->enable_profiler(TRUE);
    }

    /**
     * Liệt kê danh sách các bảo hiểm của chủ gian hàng hoặc danh sách tất cả
     * bảo hiểm nếu người đăng nhập là quản trị
     *
     * @return type
     */
    function browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {
        $options = array('lang' => switch_language($para1), 'page' => $para2);
        $options = array_merge($options, $this->_prepare_search($options));

        $options['products_sort_type'] = $this->phpsession->get('products_sort_type');
        $this->phpsession->save('product_lang', $options['lang']);
        $options['user_id'] = $this->phpsession->get('user_id');
        $options['role_id'] = $this->phpsession->get('role_id');
        $options['trademark_id'] = $this->phpsession->get('trademark_id');
        $options['is_admin'] = TRUE;

        // Get all customer bussiness parent
        $customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP));
        if (!empty($customers_b)) {
            $options['customers_b'] = $customers_b;
        }
        //end
        // Filter
        if (isset($_GET['key']) && $_GET['key'] != '') {
            $options['key'] = $_GET['key'];
        }
        if (isset($_GET['phanloai_kh']) && $_GET['phanloai_kh'] != '') {
            $options['phanloai_kh'] = $_GET['phanloai_kh'];
        }
        // Filter doanh nghiệp
        if ((isset($_GET['dn']) && $_GET['dn'] != '') || (isset($_GET['phanloai_kh']) && $_GET['phanloai_kh'] == DOANHNGHIEP)) {
            //$options['custom_buy_id'] = $_GET['custom_buy_id'];
            $dn_id = $_GET['dn'];
            $str_dn_id = $dn_id . ',';

            if (!empty($customers_b)) {
                $option_dn = '';
                foreach ($customers_b as $index) {
                    $id = $index->id;
                    $parent_id = $index->parent_id;
                    $congty_canhan = $index->congty_canhan;
                    $selected = ($_GET['dn'] == $id) ? 'selected="selected"' : '';
                    if ($parent_id == FALSE) {
                        $option_dn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                    }
                    if ($parent_id == $dn_id) {
                        $str_dn_id .= $id . ',';

                        foreach ($customers_b as $index1) {
                            $id1 = $index->id;
                            $parent_id1 = $index->parent_id;
                            if ($id == $id1) {
                                $str_dn_id .= $id1 . ',';
                            }
                        }
                    }
                }
                $str_dn_id .=$id;
                if (isset($_GET['dn']) && $_GET['dn'] != '') {
                    $options['a_custom_buy_id'] = array_unique(@explode(',', $str_dn_id));
                }
            }
            $options['selectbox_dn'] = '<select type="2" selectbox_name="dn" name="dn" class="" id="filter_doanhnghiep">
                            <option value="">Tất cả doanh nghiệp BH</option>
                            ' . $option_dn . '
                        </select>';
        }
        //end
        if ((isset($_GET['cn']) && $_GET['cn'] != '') || (isset($_GET['dn']) && $_GET['dn'] != '')) {
            if (isset($_GET['dn']) && $_GET['dn'] != '') {
                $dn_id = $_GET['dn'];
            }
            if (isset($_GET['cn']) && $_GET['cn'] != '') {
                $options['custom_buy_id'] = $_GET['cn'];
            }
            if (!empty($customers_b)) {
                $option_cn = '';
                foreach ($customers_b as $index) {
                    $id = $index->id;
                    $congty_canhan = $index->congty_canhan;
                    $parent_id = $index->parent_id;
                    $selected = ($_GET['cn'] == $id) ? 'selected="selected"' : '';
                    if ($dn_id == $parent_id) {
                        $option_cn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                    }
                }
            }
            $options['selectbox_cn'] = '<select type="2" selectbox_name="cn" name="cn" class="" id="filter_chinhanh">
                            <option value="">Các công ty con/Chi nhánh BH</option>
                            ' . $option_cn . '
                        </select>';
        }
        //end

        if ($options['cat_id'] != DEFAULT_COMBO_VALUE) {
//            $cat_array = $this->products_model->sql_get_by_cat($options['cat_id']);
//            $cat_array .= $options['cat_id'];
//            $options['cat_array'] = @explode(',', $cat_array);
        }
        $total_row = $this->products_model->get_products_count($options);
        $total_pages = (int) ($total_row / PRODUCTS_ADMIN_PRODUCT_PER_PAGE);
        if ($total_pages * PRODUCTS_ADMIN_PRODUCT_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;

        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * PRODUCTS_ADMIN_PRODUCT_PER_PAGE;
        $options['limit'] = PRODUCTS_ADMIN_PRODUCT_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['page_links'] = $this->pagination->create_ajax_links();
//        echo '<pre>';
//        print_r($options);
//        echo '</pre>';
//        die();
        $options['products'] = $this->products_model->get_products($options);
//        $options['customer_dh']   = $this->products_model->get_name_dh($options); 
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page'] = $options['page'];
        $options['product_name'] = isset($options['product_name']) ? $options['product_name'] : FALSE;
// REM 2014
//        $options['categories_array'] = $this->products_categories_model->get_categories_array();

        $options['filter'] = $options['filter'];

        if ($options['lang'] <> DEFAULT_LANGUAGE) {
            $options['uri'] = PRODUCTS_ADMIN_BASE_URL . '/' . $options['lang'];
        } else {
            $options['uri'] = PRODUCTS_ADMIN_BASE_URL;
        }

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('admin/products/product_list', $options, TRUE);

        // Chuan bi cac the META
        $this->_view_data['title'] = 'Quản lý bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _prepare_search($options = array()) {
        $view_data = array();
        // nếu submit
        if ($this->is_postback()) {
            $this->phpsession->save('product_name_search', $this->db->escape_str($this->input->post('product_name')));
            $view_data['search'] = $this->phpsession->get('product_name_search');
            $this->phpsession->save('categories_search_id', $this->input->post('categories_id'));
            $view_data['categories_combo'] = $this->products_categories_model->get_categories_combo(array('combo_name' => 'categories_id', 'categories_id' => $this->input->post('categories_id'), 'lang' => $options['lang'], 'extra' => 'class="btn" style="max-width: 25%;"'));

            $options['start_date'] = $this->input->post('start_date');
            $options['end_date'] = $this->input->post('end_date');
            if (isset($options['start_date']) && $options['start_date'] != '') {
                $options['start_date_m'] = strtotime($options['start_date']);
            }
            if (isset($options['end_date']) && $options['end_date'] != '') {
                $options['end_date_m'] = strtotime($options['end_date']);
            }
            //search with lang
            $options['lang'] = $this->input->post('lang');
            $this->phpsession->save('orders_search_options', $options);
            $view_data['start_date'] = $this->phpsession->get('start_date');
        } else {
            $temp_options = $this->phpsession->get('orders_search_options');
            if (is_array($temp_options)) {

                $options['start_date'] = $temp_options['start_date'];
                $options['end_date'] = $temp_options['end_date'];
            } else {
                $options['start_date'] = '';
                $options['end_date'] = '';
            }

            $view_data['start_date'] = $this->phpsession->get('start_date');
            $view_data['search'] = $this->phpsession->get('product_name_search');
            if (!($this->phpsession->get('categories_search_id')))
                $this->phpsession->save('categories_search_id', DEFAULT_COMBO_VALUE);
            $view_data['categories_combo'] = $this->products_categories_model->get_categories_combo(array('combo_name' => 'categories_id', 'categories_id' => $this->phpsession->get('categories_search_id'), 'lang' => $options['lang'], 'extra' => 'class="btn" style="max-width: 25%;"'));
        }
        $view_data['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $options['lang'], 'extra' => 'onchange="javascript:change_lang();" class="btn"'));
        $view_data['sort_combobox'] = $this->products_model->get_products_sort_combobox(array('sort_combobox' => $this->phpsession->get('products_sort_type'), 'extra' => 'onchange="javascript:change_sort();" class="btn"'));
        $options['keyword'] = $this->phpsession->get('product_name_search');
        $options['cat_id'] = $this->phpsession->get('categories_search_id');
        $view_data['start_date'] = $options['start_date'];
        $view_data['end_date'] = $options['end_date'];
        $options['filter'] = $this->load->view('admin/products/search_product_form', $view_data, TRUE);

        return $options;
    }

    public function do_sort_products_list() {
        if ($this->is_postback()) {
            $this->phpsession->save('products_sort_type', $this->input->post('sort_combobox'));
        } else {
            $this->phpsession->save('products_sort_type', '');
        }
    }

    /*
     * Thêm mới bảo hiểm
     */

    function add() {
        $options = array();

        if ($this->is_postback()) {
            if (!$this->_do_add_products())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        $options += $this->_get_add_products_form_data();

        // Lấy thông tin các dn bán BH
        $data_get = array(
            'parent_id' => FALSE,
        );
        $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
        if (!empty($dn_bbh)) {
            $options['a_dn_bbh'] = $dn_bbh;
        }
        //end
        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('admin/products/edit_product_form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Thêm đơn bảo hiểm' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_add_products_form_data($options = array()) {

        $view_data = array();

        // Get from DB
//        if($this->is_postback())
//        {
//        }
        // Get from submit
//        else
//        {
        $view_data['product_name'] = $this->input->post('product_name', TRUE);
        $view_data['code'] = $this->input->post('code', TRUE);
        $view_data['buy_name'] = $this->input->post('buy_name', TRUE);
        $view_data['custom_buy_id'] = $this->input->post('custom_buy_id', TRUE);
        $view_data['custom_duoc_name'] = $this->input->post('custom_duoc_name', TRUE);
        $view_data['cmt_custom_duoc'] = $this->input->post('cmt_custom_duoc', TRUE);
        $view_data['custom_duoc_id'] = $this->input->post('custom_duoc_id', TRUE);
        $view_data['products_origin'] = $this->products_origin_model->get_products_origin_combo(array('products_origin' => $this->input->post('products_origin'), 'extra' => 'class="btn"'));
        $view_data['contract_date'] = $this->input->post('contract_date', TRUE);
        $view_data['categories'] = $this->products_categories_model->get_categories_combo(array('categories_combobox' => $this->input->post('categories_combobox'), 'lang' => $this->input->post('lang'), 'extra' => 'class="btn select_t"'));
        $view_data['products_trademark'] = $this->products_trademark_model->get_products_trademark_combo(array('products_trademark' => $this->input->post('products_trademark'), 'extra' => 'class="btn" id="products_trademark"'));
        $view_data['price'] = $this->input->post('price', TRUE);
        $view_data['price_old'] = $this->input->post('price_old', TRUE);
        $view_data['unit'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit', 'unit' => $this->input->post('unit'), 'extra' => 'class="btn"'));
        $view_data['product_unit'] = $this->products_units_model->get_units_combo(array('units' => $this->input->post('units', TRUE), 'extra' => 'class="btn"'));
        $view_data['unit_old'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_old', 'unit_old' => $this->input->post('unit_old'), 'extra' => 'class="btn"'));
        $view_data['product_unit_old'] = $this->products_units_model->get_units_combo(array('units_old' => $this->input->post('units_old', TRUE), 'extra' => 'class="btn"'));
        $view_data['unit_real'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_real', 'unit_real' => $this->input->post('unit_real'), 'extra' => 'class="btn"'));
        $view_data['commission'] = $this->input->post('commission', TRUE);
        $view_data['price_real'] = $this->input->post('price_real', TRUE);
        $view_data['products_style'] = $this->products_style_model->get_products_style_combo(array('products_style' => $this->input->post('products_style'), 'extra' => 'class="btn"'));
        $view_data['date_of_payment'] = $this->input->post('date_of_payment', TRUE);
        $view_data['re_costs'] = $this->input->post('re_costs', TRUE);
        $view_data['description'] = $this->input->post('description', TRUE);
        $view_data['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->phpsession->get("product_lang"), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
        $view_data['products_state'] = $this->products_state_model->get_products_state_combo(array('products_state' => $this->input->post('products_state'), 'extra' => 'class="btn"'));
        $view_data['products_size'] = $this->products_size_model->get_products_size_combo(array('products_size' => $this->input->post('products_size'), 'extra' => 'class="btn"'));


//   }
//        $view_data['product_id']            = $product_id;
//        $this->phpsession->save('product_id', $product_id);
        //$view_data['uri']           = PRODUCTS_ADMIN_EDIT_URL;
        $view_data['submit_uri'] = PRODUCTS_ADMIN_ADD_URL;
        $view_data['submit_uri_cus'] = CUSTOM_ADMIN_BASE_URL . '/add_from_pro';
        $view_data['header'] = 'Thêm thông tin bảo hiểm';
        $view_data['button_name'] = 'Lưu dữ liệu';
//        $view_data['images']        = $this->get_products_images();
        $view_data['scripts'] = $this->_get_scripts();

        return $view_data;
    }

    private function _do_add_products() {
        $this->form_validation->set_rules('custom_buy_id', 'Người mua', 'required|trim');
        $this->form_validation->set_rules('custom_duoc_id', 'Tên người được bảo hiểm', 'required|trim');
        $this->form_validation->set_rules('dn_bbh', 'Công ty Bảo Hiểm', 'required|trim');
        $this->form_validation->set_rules('cn_bbh', 'Chi nhánh/phòng ban', 'trim');
        $this->form_validation->set_rules('goi_bh', 'Chọn gói BH', 'required|trim');
        $this->form_validation->set_rules('sohopdong', 'Số hợp đồng bảo hiểm', 'required|trim');
        $this->form_validation->set_rules('ngay_hieuluc', 'Ngày hiệu lực', 'required|trim');
        $this->form_validation->set_rules('hoahong', 'Hoa hồng', 'required|trim');
        $this->form_validation->set_rules('phi_bbh', 'Phí của cty bảo hiểm', 'required|trim');
        $this->form_validation->set_rules('ngay_thanhtoan', 'Ngày thanh toán', 'required|trim');
        $this->form_validation->set_rules('ghichu', 'Ghi chú bảo hiểm', 'trim');


//            $this->form_validation->set_rules('products_origin', 'Nguồn khách hàng', 'is_not_default_combo');
//            $this->form_validation->set_rules('contract_date', 'Ngày hiệu lực', 'required|trim');
//            $this->form_validation->set_rules('products_trademark', 'Nhà cung cấp', 'is_not_default_combo');
//            $this->form_validation->set_rules('categories_combobox', 'Loại hình bảo hiểm', 'required|is_not_default_combo');
//            $this->form_validation->set_rules('commission', 'Hoa hồng', 'required|trim');
//            $this->form_validation->set_rules('price', 'Phí của cty bảo hiểm', 'required|trim|max_length[11]|xss_clean|is_numeric');
//            $this->form_validation->set_rules('price_old', 'Phí phải thu KH', 'required|trim|max_length[11]|xss_clean|is_numeric');
//            $this->form_validation->set_rules('price_real', 'Số tiền thực thu', 'required|trim|max_length[11]|xss_clean|is_numeric');
//            $this->form_validation->set_rules('products_style', 'Hình thức thanh toán', 'is_not_default_combo');
//            $this->form_validation->set_rules('date_of_payment', 'Ngày thanh toán', 'required|trim');
//            $this->form_validation->set_rules('re_costs', 'Còn lại', 'required|trim');

        if ($this->form_validation->run($this)) {
            $product_data = array(
                'product_name' => strip_tags($this->input->post('product_name', TRUE)),
                'custom_buy_id' => $this->input->post('custom_buy_id', TRUE),
                'custom_duoc_id' => $this->input->post('custom_duoc_id', TRUE),
                'size' => $this->input->post('products_size', TRUE),
                'buy_name' => $this->input->post('buy_name', TRUE),
                'custom_duoc_name' => $this->input->post('custom_duoc_name', TRUE),
                'cmt_custom_duoc' => $this->input->post('cmt_custom_duoc', TRUE),
                'origin_id' => $this->input->post('products_origin', TRUE),
                'code' => my_trim($this->input->post('code', TRUE)),
                'categories_id' => $this->input->post('categories_combobox', TRUE),
                'trademark_id' => $this->input->post('products_trademark', TRUE),
                'price' => $this->input->post('price'),
                'price_old' => $this->input->post('price_old'),
                'commission' => $this->input->post('commission', TRUE),
                'price_real' => $this->input->post('price_real'),
                're_costs' => $this->input->post('re_costs', TRUE),
                'description' => $content,
                'updated_date' => date('Y-m-d H:i:s'),
                'lang' => $this->input->post('lang', TRUE),
//                    'unit_id'       => $this->input->post('units', TRUE),
//                    'unit_id_old'   => $this->input->post('units_old', TRUE),
//                    'unit_id_real'   => $this->input->post('units_real', TRUE),
                'editor' => $this->phpsession->get('user_id'),
                'style_id' => $this->input->post('products_style', TRUE),
                //end
                'dn_bbh' => $this->input->post('dn_bbh', TRUE),
                'cn_bbh' => $this->input->post('cn_bbh', TRUE),
                'goi_bh' => $this->input->post('goi_bh', TRUE),
                'sohopdong' => $this->input->post('sohopdong', TRUE),
                //'ngay_hieuluc' => $this->input->post('ngay_hieuluc', TRUE),
                'hoahong' => $this->input->post('hoahong', TRUE),
                'phi_bbh' => $this->input->post('phi_bbh', TRUE),
                //'ngay_thanhtoan' => $this->input->post('ngay_thanhtoan', TRUE),
                'ghichu' => $this->input->post('ghichu', TRUE),
                    //end
            );
            $ngay_thanhtoan = datetimepicker_array2($this->input->post('ngay_thanhtoan', TRUE));
            $product_data['ngay_thanhtoan'] = date('Y-m-d H:i:s', mktime($ngay_thanhtoan['hour'], $ngay_thanhtoan['minute'], $ngay_thanhtoan['second'], $ngay_thanhtoan['month'], $ngay_thanhtoan['day'], $ngay_thanhtoan['year']));
            $ngay_hieuluc = datetimepicker_array2($this->input->post('ngay_hieuluc', TRUE));
            $product_data['ngay_hieuluc'] = date('Y-m-d H:i:s', mktime($ngay_hieuluc['hour'], $ngay_hieuluc['minute'], $ngay_hieuluc['second'], $ngay_hieuluc['month'], $ngay_hieuluc['day'], $ngay_hieuluc['year']));
            //end
            $contract_date = datetimepicker_array2($this->input->post('contract_date', TRUE));
            $product_data['contract_date'] = date('Y-m-d H:i:s', mktime($contract_date['hour'], $contract_date['minute'], $contract_date['second'], $contract_date['month'], $contract_date['day'], $contract_date['year']));
            $date_of_payment = datetimepicker_array2($this->input->post('date_of_payment', TRUE));
            $product_data['date_of_payment'] = date('Y-m-d H:i:s', mktime($date_of_payment['hour'], $date_of_payment['minute'], $date_of_payment['second'], $date_of_payment['month'], $date_of_payment['day'], $date_of_payment['year']));
            $product_data['update_time'] = now();
            $position_add = $this->products_model->position_to_add_products(array('lang' => $data['lang']));
            $product_data['position'] = $position_add;

            $this->products_model->insert($product_data);

//                if(SLUG_ACTIVE>0){
//                    $this->slug_model->update_slug(array('slug'=>  my_trim($this->input->post('slug', TRUE)).SLUG_CHARACTER_URL,'type'=>SLUG_TYPE_PRODUCTS,'type_id'=>$product_data['id']));
//                }
//                if($this->input->get('back_url') != '')
//                    redirect($this->input->get('back_url'));
//                else
            redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $product_data['lang']);
        }
        $this->_last_message = validation_errors();
        return FALSE;
    }

    private function _do_add_products1() {
        $this->form_validation->set_rules('title', 'Tiêu đề', 'trim|required|xss_clean|max_length[255]');
        if (SLUG_ACTIVE > 0) {
            $this->form_validation->set_rules('slug', 'Slug', 'trim|required|xss_clean|max_length[1000]');
        }
        $this->form_validation->set_rules('categories_combobox', 'Phân loại tin', 'is_not_default_combo');
        $this->form_validation->set_rules('thumb', 'Hình minh họa', 'trim|required|xss_clean');
        $this->form_validation->set_rules('summary', 'Tóm tắt', 'trim|required|xss_clean|max_length[512]');
        $this->form_validation->set_rules('content', 'Nội dung', 'required');
        $this->form_validation->set_rules('created_date', 'Ngày đăng tin', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('security_code', 'Mã an toàn', 'trim|required|xss_clean|matches_value[' . $this->phpsession->get('captcha') . ']');

        if ($this->form_validation->run()) {
            $post_data = $this->_get_posted_products_data();
            $post_data['creator'] = $this->phpsession->get('user_id');
            $position_add = $this->products_model->position_to_add_products(array('lang' => $post_data['lang']));
            $post_data['position'] = $position_add;
            $insert_id = $this->products_model->insert($post_data);
            if (SLUG_ACTIVE > 0) {
                if (isset($insert_id)) {
                    $this->slug_model->insert_slug(array('slug' => my_trim($this->input->post('slug', TRUE)) . SLUG_CHARACTER_URL, 'type' => SLUG_TYPE_NEWS, 'type_id' => $insert_id));
                }
            }
            redirect(NEWS_ADMIN_BASE_URL . '/' . $post_data['lang']);
        }
        return FALSE;
    }

    function add_pro() {
        $options = array();
        $this->form_validation->set_rules('code', 'Số hợp đồng', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run()) {
            $data = array(
                'product_name' => $this->input->post('product_name'),
                'status' => STATUS_ACTIVE,
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => date('Y-m-d H:i:s'),
                'update_time' => now(),
                'lang' => $this->phpsession->get("product_lang"),
                'creator' => $this->phpsession->get('user_id'),
                'editor' => $this->phpsession->get('user_id'),
            );
            $position_add = $this->products_model->position_to_add_products(array('lang' => $data['lang']));
            $data['position'] = $position_add;
            $product_id = $this->products_model->insert($data);
            //redirect(PRODUCTS_ADMIN_EDIT_URL .'/'. $product_id);
            return $this->edit(array('id' => $product_id));
        } else {
            $options['error'] = validation_errors();
            $options['header'] = 'Thêm bảo hiểm';

            if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
                $options['options'] = $options;

            // Chuan bi du lieu chinh de hien thi
            $this->_view_data['main_content'] = $this->load->view('admin/products/edit_product_form', $options, TRUE);
            // Chuan bi cac the META
            $this->_view_data['title'] = 'Thêm bảo hiểm' . DEFAULT_TITLE_SUFFIX;
            // Tra lai view du lieu cho nguoi su dung
            $this->load->view($this->_layout, $this->_view_data);
        }
        return FALSE;
    }

    /**
     * Thực hiện việc sửa nội dung bảo hiểm.
     *
     */
    function edit($options = array()) {
//        $this->output->link_js('/powercms/scripts/uploadify/jquery.uploadify-3.1.min.js');
//        $this->output->link_js('/powercms/scripts/jquery/ui.sortable.js');
//        $this->output->javascripts('uploadify();');
//        $this->output->javascripts('setup_moveable();');
//        $this->output->javascripts('set_hover_img();');
//        if(!$this->is_postback()) redirect(PRODUCTS_ADMIN_BASE_URL);

        if ($this->is_postback() && !$this->input->post('from_list')) {
            if (!$this->_do_edit_product())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }

        if (!isset($options['id'])) {
            $options['id'] = NULL;
        }

        $options += $this->_get_edit_product_form_data(array('id' => $options['id']));
        //end
        $bh_id = $this->input->post('id');
        // Lấy thông tin BH
        $bh = $this->products_model->get_products(array('id' => $bh_id));
        if (is_object($bh)) {
            $dn_bbh_id = $bh->dn_bbh;

            // Lấy thông tin các dn bán BH
            $data_get = array(
                    //'parent_id' => FALSE,
            );
            $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
            if (!empty($dn_bbh)) {
                $options['a_dn_bbh'] = $dn_bbh;
            }
            //end
            // Lấy các gói BH của cn bbh
            $data_get = array(
                'dn_bbh' => $dn_bbh_id,
            );
//        echo '<pre>';
//        print_r($data_get);
//        die;
            $this->load->model('products/products_size_model');
            $a_goi_bh = $this->products_size_model->get_products_size($data_get);
            if(!empty($a_goi_bh)){
                $options['a_goi_bh']= $a_goi_bh;
            }
        }

        //end
        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('admin/products/edit_product_form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Sửa bảo hiểm' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_edit_product_form_data($options = array()) {
        if (isset($options['id']))
            $product_id = $options['id'];
        else
            $product_id = $this->input->post('id');

        $view_data = array();

        // Get from DB
        if ($this->input->post('from_list')) {
            $products = $this->products_model->get_products(array('id' => $product_id, 'is_admin' => TRUE));
            if (!is_object($products))
                show_404();
            $view_data['product_name'] = $products->product_name;

            $view_data['products'] = $products;
            $view_data['code'] = $products->code;
            $custom_buy = $this->customers_model->get_customers_buy_id(array('id' => $products->custom_buy_id));
            $view_data['custom_buy_name'] = $custom_buy->fullname;
            $view_data['custom_buy_id'] = $custom_buy->id;

            $custom_duoc = $this->customers_model->get_customers_buy_id(array('id' => $products->custom_duoc_id));
            $view_data['custom_duoc_name'] = $custom_duoc->fullname;
            $view_data['cmt_custom_duoc'] = $custom_duoc->cmt;
            $view_data['custom_duoc_id'] = $custom_duoc->id;

//            $view_data['custom_buy_id']     = $this->customers_model->get_customers_buy_combo(array('custom_buy_id' => $products->custom_buy_id, 'extra' => 'class="btn"'));
//            $view_data['custom_duoc_id']    = $this->customers_model->get_customers_duoc_combo(array('custom_duoc_id' => $products->custom_duoc_id, 'extra' => 'class="btn"'));
            $view_data['products_origin'] = $this->products_origin_model->get_products_origin_combo(array('products_origin' => $products->origin_id, 'extra' => 'class="btn"'));
            $view_data['contract_date'] = $products->contract_date;
            $view_data['categories'] = $this->products_categories_model->get_categories_combo(array('categories_combobox' => $products->categories_id, 'trade_id' => $products->trademark_id, 'lang' => $products->lang, 'extra' => 'class="btn select_t"'));
            $view_data['products_trademark'] = $this->products_trademark_model->get_products_trademark_combo(array('products_trademark' => $products->trademark_id, 'extra' => 'class="btn" id="products_trademark"'));

            $price_unit = get_unit_from_price($products->price);
            $view_data['price'] = str_replace('.00', '', $price_unit['price']);

            $price_unit_old = get_unit_from_price($products->price_old);
            $view_data['price_old'] = str_replace('.00', '', $price_unit_old['price']);

            $price_real = get_unit_from_price($products->price_real);
            $view_data['price_real'] = str_replace('.00', '', $price_real['price']);

            $view_data['unit'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit', 'unit' => $price_unit['unit'], 'extra' => 'class="btn"'));
            $view_data['product_unit'] = $this->products_units_model->get_units_combo(array('units' => $products->unit_id, 'extra' => 'class="btn"'));
            $view_data['unit_old'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_old', 'unit_old' => $price_unit_old['unit'], 'extra' => 'class="btn"'));
            $view_data['product_unit_old'] = $this->products_units_model->get_units_combo(array('units_old' => $products->unit_id_old, 'extra' => 'class="btn"'));

            $view_data['unit_real'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_real', 'unit_real' => $price_real['unit'], 'extra' => 'class="btn"'));
            $view_data['commission'] = $products->commission;

            $view_data['products_style'] = $this->products_style_model->get_products_style_combo(array('products_style' => $products->style_id, 'extra' => 'class="btn"'));
            $view_data['date_of_payment'] = $products->date_of_payment;
            $view_data['re_costs'] = $products->re_costs;
            $view_data['description'] = $products->description;


//            $view_data['typical_product']   = $this->utility_model->get_special_product_combo(array('combo_name' => 'typical', 'typical'=> $products->typical, 'extra' => 'class="btn"'));
//            $view_data['new_product']       = $this->utility_model->get_special_product_combo(array('combo_name' => 'new', 'new'=> $products->new, 'extra' => 'class="btn"'));
//            $view_data['top_sell_product']  = $this->utility_model->get_special_product_combo(array('combo_name' => 'top_seller', 'top_seller'=> $products->top_seller, 'extra' => 'class="btn"'));
//            $view_data['meta_keywords']     = $products->meta_keywords;
//            $view_data['meta_description']  = $products->meta_description;
//            $view_data['link_demo']         = $products->link_demo;
            $view_data['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $products->lang, 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
//            $colors = explode(',', $products->colors);
//            $view_data['products_color']    = $this->products_color_model->get_products_color_combo(array('products_color[]' => $colors, 'extra' => 'multiple="multiple" class="btn" size="3" style="height: 120px; width: 300px; padding-left: 5px;"'));
            $view_data['products_state'] = $this->products_state_model->get_products_state_combo(array('products_state' => $products->state_id, 'extra' => 'class="btn"'));

            $view_data['products_size'] = $this->products_size_model->get_products_size_combo(array('products_size' => $products->size, 'extra' => 'class="btn" '));

//            $view_data['products_material'] = $this->products_material_model->get_products_material_combo(array('products_material' => $products->material_id, 'extra' => 'class="btn"'));
        }
        // Get from submit
        else {
            $view_data['product_name'] = $this->input->post('product_name', TRUE);
//            if(SLUG_ACTIVE>0){
//                $view_data['slug'] = my_trim($this->input->post('slug', TRUE));
//            }
//            $view_data['summary']           = '';
            $view_data['code'] = $this->input->post('code', TRUE);
            $view_data['custom_buy_id'] = $this->input->post('custom_buy_id', TRUE);
            $view_data['custom_duoc_id'] = $this->input->post('custom_duoc_id', TRUE);

//            $view_data['custom_buy_id']     = $this->customers_model->get_customers_buy_combo(array('customers_buy' => $this->input->post('customers_buy'), 'extra' => 'class="btn"'));
//            $view_data['custom_duoc_id']    = $this->customers_model->get_customers_duoc_combo(array('custom_duoc_id' => $this->input->post('custom_duoc_id'), 'extra' => 'class="btn"'));
            $view_data['products_origin'] = $this->products_origin_model->get_products_origin_combo(array('products_origin' => $this->input->post('products_origin'), 'extra' => 'class="btn"'));
            $view_data['contract_date'] = $this->input->post('contract_date', TRUE);
            $view_data['categories'] = $this->products_categories_model->get_categories_combo(array('categories_combobox' => $this->input->post('categories_combobox'), 'lang' => $this->input->post('lang'), 'extra' => 'class="btn select_t"'));
            $view_data['products_trademark'] = $this->products_trademark_model->get_products_trademark_combo(array('products_trademark' => $this->input->post('products_trademark'), 'extra' => 'class="btn" id="products_trademark"'));


            $view_data['price'] = $this->input->post('price', TRUE);
            $view_data['price_old'] = $this->input->post('price_old', TRUE);
            $view_data['unit'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit', 'unit' => $this->input->post('unit'), 'extra' => 'class="btn"'));
            $view_data['product_unit'] = $this->products_units_model->get_units_combo(array('units' => $this->input->post('units', TRUE), 'extra' => 'class="btn"'));
            $view_data['unit_old'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_old', 'unit_old' => $this->input->post('unit_old'), 'extra' => 'class="btn"'));
            $view_data['product_unit_old'] = $this->products_units_model->get_units_combo(array('units_old' => $this->input->post('units_old', TRUE), 'extra' => 'class="btn"'));

            $view_data['unit_real'] = $this->utility_model->get_product_units_combo(array('combo_name' => 'unit_real', 'unit_real' => $this->input->post('unit_real'), 'extra' => 'class="btn"'));
            $view_data['commission'] = $this->input->post('commission', TRUE);
            $view_data['price_real'] = $this->input->post('price_real', TRUE);
            $view_data['products_style'] = $this->products_style_model->get_products_style_combo(array('products_style' => $this->input->post('products_style'), 'extra' => 'class="btn"'));
            $view_data['date_of_payment'] = $this->input->post('date_of_payment', TRUE);
            $view_data['re_costs'] = $this->input->post('re_costs', TRUE);
            $view_data['description'] = $this->input->post('description', TRUE);

//            $view_data['typical_product']   = $this->utility_model->get_special_product_combo(array('combo_name' => 'typical', 'typical'=> $this->input->post('typical'), 'extra' => 'class="btn"'));
//            $view_data['new_product']       = $this->utility_model->get_special_product_combo(array('combo_name' => 'new', 'new'=> $this->input->post('new'), 'extra' => 'class="btn"'));
//            $view_data['top_sell_product']  = $this->utility_model->get_special_product_combo(array('combo_name' => 'top_seller', 'top_seller'=> $this->input->post('top_seller'), 'extra' => 'class="btn"'));
//            $view_data['description']       = '';
//            $view_data['manufacturer']      = '';
//            $view_data['specifications']    = '';
//            $view_data['tags']              = $this->input->post('tags', TRUE);
//            $view_data['meta_title']        = $this->input->post('meta_title', TRUE);
//            $view_data['meta_keywords']     = $this->input->post('meta_keywords', TRUE);
//            $view_data['meta_description']  = $this->input->post('meta_description', TRUE);
//            $view_data['link_demo']         = $this->input->post('link_demo', TRUE);
            $view_data['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->phpsession->get("product_lang"), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
//            $colors = $this->input->post('products_color',TRUE);
//            $view_data['products_color']    = $this->products_color_model->get_products_color_combo(array('products_color[]' => $colors, 'extra' => 'multiple="multiple" class="btn" size="3" style="height: 120px; width: 300px; padding-left: 5px;"'));
            $view_data['products_state'] = $this->products_state_model->get_products_state_combo(array('products_state' => $this->input->post('products_state'), 'extra' => 'class="btn"'));

            $view_data['products_size'] = $this->products_size_model->get_products_size_combo(array('products_size' => $this->input->post('products_size'), 'extra' => ' class="btn" '));
//            $view_data['products_style']    = $this->products_style_model->get_products_style_combo(array('products_style' => $this->input->post('products_style'), 'extra' => 'class="btn"'));
//            $view_data['products_material'] = $this->products_material_model->get_products_material_combo(array('products_material' => $this->input->post('products_material'), 'extra' => 'class="btn"'));
        }
        $view_data['product_id'] = $product_id;
        $this->phpsession->save('product_id', $product_id);
        //$view_data['uri']           = PRODUCTS_ADMIN_EDIT_URL;
        $view_data['submit_uri'] = PRODUCTS_ADMIN_EDIT_URL;
        $view_data['submit_uri_cus'] = CUSTOM_ADMIN_BASE_URL . '/add_from_pro';
        $view_data['header'] = 'Sửa thông tin bảo hiểm';
        $view_data['button_name'] = 'Lưu dữ liệu';
//        $view_data['images']        = $this->get_products_images();
        $view_data['scripts'] = $this->_get_scripts();

        return $view_data;
    }

    /**
     * Thực hiện việc thêm bảo hiểm vào trong DB
     * @return type
     */
    private function _do_edit_product() {
        if ($this->input->post('btnSubmit') === 'Lưu dữ liệu') {

            $this->form_validation->set_rules('custom_duoc_name', 'Tên người được bảo hiểm', 'trim|required|max_length[255]|xss_clean');
            $this->form_validation->set_rules('custom_buy_id', 'Người mua', 'required|trim');
            $this->form_validation->set_rules('custom_duoc_id', 'Tên người được bảo hiểm', 'required|trim');
            $this->form_validation->set_rules('dn_bbh', 'Công ty Bảo Hiểm', 'required|trim');
            $this->form_validation->set_rules('cn_bbh', 'Chi nhánh/phòng ban', 'trim');
            $this->form_validation->set_rules('goi_bh', 'Chọn gói BH', 'required|trim');
            $this->form_validation->set_rules('sohopdong', 'Số hợp đồng bảo hiểm', 'required|trim');
            $this->form_validation->set_rules('ngay_hieuluc', 'Ngày hiệu lực', 'required|trim');
            $this->form_validation->set_rules('hoahong', 'Hoa hồng', 'required|trim');
            $this->form_validation->set_rules('phi_bbh', 'Phí của cty bảo hiểm', 'required|trim');
            $this->form_validation->set_rules('ngay_thanhtoan', 'Ngày thanh toán', 'required|trim');
            $this->form_validation->set_rules('ghichu', 'Ghi chú bảo hiểm', 'trim');
//            if(SLUG_ACTIVE>0){
//                $this->form_validation->set_rules('slug', 'Slug', 'trim|required|xss_clean|max_length[1000]');
//            }
//            $this->form_validation->set_rules('categories_combobox', 'Phân loại bảo hiểm', 'required|is_not_default_combo');
//            $this->form_validation->set_rules('price', 'Giá tiền', 'trim|max_length[11]|xss_clean|is_numeric');
//            $this->form_validation->set_rules('price_old', 'Giá cũ', 'trim|max_length[11]|xss_clean|is_numeric');
//            $this->form_validation->set_rules('summary', 'Mô tả ngắn', 'trim|xss_clean');
//            $this->form_validation->set_rules('code', 'Mã bảo hiểm', 'trim|xss_clean|max_length[255]');
//            $this->form_validation->set_rules('description', 'Thông tin bảo hiểm', 'trim|min_length[10]');
//            $this->form_validation->set_rules('manufacturer', 'Thông tin nhà sản xuất', 'trim|min_length[10]');
//            $this->form_validation->set_rules('specifications', 'Thông số kỹ thuật', 'trim|min_length[10]');
//            $this->form_validation->set_rules('products_origin', 'Xuất xứ', 'is_not_default_combo');
//            $this->form_validation->set_rules('products_trademark', 'Thương hiệu', 'is_not_default_combo');
//            $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|xss_clean|max_length[255]');
//            $this->form_validation->set_rules('meta_keywords', 'Meta keywords', 'trim|xss_clean|max_length[255]');
//            $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|xss_clean');
//            $this->form_validation->set_rules('link_demo', 'Link demo', 'trim|xss_clean|max_length[500]');
//            $this->form_validation->set_rules('tags', 'Tags', 'trim|xss_clean');
//            $this->form_validation->set_rules('products_colors[]', 'Màu sắc', 'is_not_default_combo');
//            $this->form_validation->set_rules('products_size[]', 'Kích cỡ', 'is_not_default_combo');
//            $this->form_validation->set_rules('products_state', 'Tình trạng bảo hiểm', 'is_not_default_combo');
//            $this->form_validation->set_rules('products_style', 'Kiểu dáng', 'is_not_default_combo');
//            $this->form_validation->set_rules('products_material', 'Chất liệu', 'is_not_default_combo');

            if ($this->form_validation->run($this)) {
//                $content = str_replace('&lt;', '<', $this->input->post('description'));
//                $content = str_replace('&gt;', '>', $content);
//                $manufacturer = str_replace('&lt;', '<', $this->input->post('manufacturer'));
//                $manufacturer = str_replace('&gt;', '>', $manufacturer);
//                
//                $specifications = str_replace('&lt;', '<', $this->input->post('specifications'));
//                $specifications = str_replace('&gt;', '>', $specifications);
//                
//                $colors = ',' . @implode(",", $this->input->post('products_color', TRUE)) . ',';
//                
//                $size = ',' . @implode(",", $this->input->post('products_size', TRUE)) . ',';

                $product_data = array(
                    'id' => $this->input->post('id'),
                    'product_name' => strip_tags($this->input->post('product_name', TRUE)),
                    'custom_buy_id' => $this->input->post('custom_buy_id', TRUE),
                    'custom_duoc_id' => $this->input->post('custom_duoc_id', TRUE),
                    'buy_name' => $this->input->post('buy_name', TRUE),
                    'custom_duoc_name' => $this->input->post('custom_duoc_name', TRUE),
                    'cmt_custom_duoc' => $this->input->post('cmt_custom_duoc', TRUE),
                    'origin_id' => $this->input->post('products_origin', TRUE),
                    'code' => my_trim($this->input->post('code', TRUE)),
                    'size' => $this->input->post('products_size', TRUE),
                    'categories_id' => $this->input->post('categories_combobox', TRUE),
                    'trademark_id' => $this->input->post('products_trademark', TRUE),
                    'price' => $this->input->post('price'),
                    'price_old' => $this->input->post('price_old'),
                    'commission' => $this->input->post('commission', TRUE),
                    'price_real' => $this->input->post('price_real'),
//                    'date_of_payment' => $this->input->post('date_of_payment', TRUE),
                    're_costs' => $this->input->post('re_costs', TRUE),
                    'description' => $content,
                    'updated_date' => date('Y-m-d H:i:s'),
//                    'manufacturer'  => $manufacturer,
//                    'specifications'=> $specifications,
//                    
//                    'summary'       => my_trim($this->input->post('summary', TRUE)),
//                    
//                    'typical'       => $this->input->post('typical', TRUE),
//                    'new'           => $this->input->post('new', TRUE),
//                    'top_seller'    => $this->input->post('top_seller', TRUE),
                    'lang' => $this->input->post('lang', TRUE),
                    'unit_id' => $this->input->post('units', TRUE),
                    'unit_id_old' => $this->input->post('units_old', TRUE),
                    'unit_id_real' => $this->input->post('units_real', TRUE),
//                    'link_demo'     => $this->input->post('link_demo', TRUE),
                    'editor' => $this->phpsession->get('user_id'),
//                    'colors'        => $colors,
//                    'size'          => $size,
//                    'state_id'      => $this->input->post('products_state', TRUE),
                    'style_id' => $this->input->post('products_style', TRUE),
//                    'material_id'   => $this->input->post('products_material', TRUE),
                    //end
                    'dn_bbh' => $this->input->post('dn_bbh', TRUE),
                    'cn_bbh' => $this->input->post('cn_bbh', TRUE),
                    'goi_bh' => $this->input->post('goi_bh', TRUE),
                    'sohopdong' => $this->input->post('sohopdong', TRUE),
                    //'ngay_hieuluc' => $this->input->post('ngay_hieuluc', TRUE),
                    'hoahong' => $this->input->post('hoahong', TRUE),
                    'phi_bbh' => $this->input->post('phi_bbh', TRUE),
                    //'ngay_thanhtoan' => $this->input->post('ngay_thanhtoan', TRUE),
                    'ghichu' => $this->input->post('ghichu', TRUE),
                        //end
                );
                $ngay_thanhtoan = datetimepicker_array2($this->input->post('ngay_thanhtoan', TRUE));
                $product_data['ngay_thanhtoan'] = date('Y-m-d H:i:s', mktime($ngay_thanhtoan['hour'], $ngay_thanhtoan['minute'], $ngay_thanhtoan['second'], $ngay_thanhtoan['month'], $ngay_thanhtoan['day'], $ngay_thanhtoan['year']));
                $ngay_hieuluc = datetimepicker_array2($this->input->post('ngay_hieuluc', TRUE));
                $product_data['ngay_hieuluc'] = date('Y-m-d H:i:s', mktime($ngay_hieuluc['hour'], $ngay_hieuluc['minute'], $ngay_hieuluc['second'], $ngay_hieuluc['month'], $ngay_hieuluc['day'], $ngay_hieuluc['year']));
                //end
                $contract_date = datetimepicker_array2($this->input->post('contract_date', TRUE));
                $product_data['contract_date'] = date('Y-m-d H:i:s', mktime($contract_date['hour'], $contract_date['minute'], $contract_date['second'], $contract_date['month'], $contract_date['day'], $contract_date['year']));
                $date_of_payment = datetimepicker_array2($this->input->post('date_of_payment', TRUE));
                $product_data['date_of_payment'] = date('Y-m-d H:i:s', mktime($date_of_payment['hour'], $date_of_payment['minute'], $date_of_payment['second'], $date_of_payment['month'], $date_of_payment['day'], $date_of_payment['year']));
                $product_data['update_time'] = now();
//                $position_edit = $this->products_model->position_to_edit_products(array('id'=>$product_data['id'],'lang'=>$product_data['lang']));
//                $product_data['position'] = $position_edit;
//                echo "<pre>";
//                print_r($product_data);
//                echo "</pre>";
//                exit();

                $this->products_model->update($product_data);

//                if($this->input->get('back_url') != '')
//                    redirect($this->input->get('back_url'));
//                else
                redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $product_data['lang']);
            }
            $this->_last_message = validation_errors();
            return FALSE;
        } else if ($this->input->post('btnSubmit') == 'Upload') {
            $this->upload_images();
            $this->_last_message = $this->products_images_model->get_last_message();
        }
    }

    /**
     * Lấy ảnh tương ứng với từng bảo hiểm
     */
    function get_products_images() {
        $options['product_id'] = $this->phpsession->get('product_id');
        $images = $this->products_images_model->get_images($options);
        $view_data = array();
        $view_data['images'] = $images;
        if ($this->input->post('is-ajax'))
            echo $this->load->view('admin/products/products_images', $view_data, TRUE);
        else
            return $this->load->view('admin/products/products_images', $view_data, TRUE);
    }

    /*
     * Thực hiện upload ảnh khách sạn = uploadify
     */

    public function ajax_upload_products_image() {
        if (!empty($_FILES)) {
            $image_path = './images/products/';
            $product_id = $this->phpsession->get('product_id');
            $products = $this->products_model->get_products(array('id' => $product_id, 'is_admin' => TRUE));
            $product_name = url_title($products->product_name, 'dash', TRUE);
            $this->products_images_model->upload_image_1($product_id, $product_name, $image_path);
        }
    }

    function upload_images() {
        $image_path = './images/products/';
        $product_id = $this->phpsession->get('product_id');
        $products = $this->products_model->get_products(array('id' => $product_id, 'is_admin' => TRUE));
        $product_name = url_title($products->product_name, 'dash', TRUE);
        $this->products_images_model->upload_image_1($product_id, $product_name, $image_path);
    }

    public function sort_products_image() {
        $arr = $this->input->post('id');
        $i = 1;
        foreach ($arr as $recordidval) {
            $array = array('position' => $i);
            $this->db->where('id', $recordidval);
            $this->db->where('products_id', $this->phpsession->get('product_id'));
            $this->db->update('product_images', $array);
            $i = $i + 1;
        }
    }

    public function delete_products_image() {
        $image_id = $this->input->post('id');
        $image_path = './images/products/';
        $this->products_images_model->delete_image($image_id, $image_path);
        echo $this->get_products_images();
    }

    /**
     * Xóa bảo hiểm
     */
    function delete() {
        $options = array();
        if ($this->is_postback()) {
            $product_id = $this->input->post('id');
            if (SLUG_ACTIVE > 0) {
                $check_slug = $this->slug_model->get_slug(array('type_id' => $product_id, 'type' => SLUG_TYPE_PRODUCTS, 'onehit' => TRUE));
                if (!empty($check_slug)) {
                    $this->slug_model->delete($check_slug->id);
                }
            }
            $this->products_images_model->delete_all_images($product_id, './images/products/');
            $this->products_model->delete($product_id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get("product_lang");
        //return $this->browse($options);
        redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $lang);
    }

//    public function up()
//    {
//        $product_id = $this->input->post('id');
//
//        // Chỉ thực hiện nếu lời gọi này được gọi từ trang list
//        //$product_id = $options['id'];
//        //$this->products_model->up_product($product_id);
//        $this->products_model->update(array('id'=>$product_id,'updated_date'=>date('Y-m-d H:i:s')));
//        $lang = $this->phpsession->get("product_lang");
//        redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $lang);
//    }
    public function up() {
        $id = $this->input->post('id');
        $this->products_model->item_to_sort_products(array('id' => $id));
        $lang = $this->phpsession->get("product_lang");
        redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $lang);
    }

    function change_status() {
        $id = $this->input->post('id');
        $product = $this->products_model->get_products(array('id' => $id, 'is_admin' => TRUE));
        $status = $product->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->products_model->update(array('id' => $id, 'status' => $status));
    }

    function change_state() {
        $id = $this->input->post('id');

        $product = $this->products_model->get_products(array('id' => $id, 'is_admin' => TRUE));
        $state = $product->state_id == STATE_ACTIVE ? STATE_INACTIVE : STATE_ACTIVE;
        $this->products_model->update(array('id' => $id, 'state_id' => $state));
    }

    function change_home() {
        $id = $this->input->post('id');
        $product = $this->products_model->get_products(array('id' => $id));
        $home = $product->home == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->products_model->update(array('id' => $id, 'home' => $home));
    }

    function export($options = array()) {

        $options['user_id'] = $this->phpsession->get('user_id');
        $options['role_id'] = $this->phpsession->get('role_id');
        $options['trademark_id'] = $this->phpsession->get('trademark_id');
        $temp_options = $this->phpsession->get('orders_search_options');
        if (is_array($temp_options)) {

            $options['start_date_m'] = $temp_options['start_date_m'];
            $options['end_date_m'] = $temp_options['end_date_m'];
        } else {
            $options['start_date_m'] = '';
            $options['end_date_m'] = '';
        }

        $options['is_admin'] = TRUE;

        $orders = $this->products_model->get_products($options);


        if (count($orders) > 0) {
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Báo cáo bảo hiểm');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'NHÂN VIÊN');
            $this->excel->getActiveSheet()->setCellValue('C1', 'CỘNG TÁC VIÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'NGÀY BÁO CÁO');
            $this->excel->getActiveSheet()->setCellValue('E1', 'NGƯỜI MUA BH');
            $this->excel->getActiveSheet()->setCellValue('F1', 'NGƯỜI ĐƯỢC BH');
            $this->excel->getActiveSheet()->setCellValue('G1', 'PHONE');
            $this->excel->getActiveSheet()->setCellValue('H1', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('I1', 'DOB');
            $this->excel->getActiveSheet()->setCellValue('J1', 'ĐỊA CHỈ');
            $this->excel->getActiveSheet()->setCellValue('K1', 'NGUỒN KH');
            $this->excel->getActiveSheet()->setCellValue('L1', 'SỐ HĐ');
            $this->excel->getActiveSheet()->setCellValue('M1', 'NGÀY HL');
            $this->excel->getActiveSheet()->setCellValue('N1', 'NHÀ BH');
            $this->excel->getActiveSheet()->setCellValue('O1', 'LOẠI BH');
            $this->excel->getActiveSheet()->setCellValue('P1', 'PHÍ CHUẨN');
            $this->excel->getActiveSheet()->setCellValue('Q1', 'PHÍ PHẢI THU KH');
            $this->excel->getActiveSheet()->setCellValue('R1', 'HOA HỒNG');
            $this->excel->getActiveSheet()->setCellValue('S1', 'SỐ TIỀN');
            $this->excel->getActiveSheet()->setCellValue('T1', 'HÌNH THỨC TT');
            $this->excel->getActiveSheet()->setCellValue('U1', 'NGÀY TT');
            $this->excel->getActiveSheet()->setCellValue('V1', 'CÒN LẠI');
            $this->excel->getActiveSheet()->setCellValue('W1', 'GHI CHÚ');

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('W1')->getFont()->setBold(true);


            $row = 2;
            foreach ($orders as $order):

                $options['custom_duoc_id'] = $order->custom_duoc_id;
                $custom_duoc = $this->products_model->get_name_dh($options);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->creator_username);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $order->product_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->created_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->fullname_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $custom_duoc->fullname_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $custom_duoc->phone_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $custom_duoc->email_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $custom_duoc->DOB_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $custom_duoc->address_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $order->origin_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $order->code);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $order->contract_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $order->trademark_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $order->category);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $order->price);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, $row, $order->price_old);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(17, $row, $order->commission);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, $row, $order->price_real);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(19, $row, $order->style_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, $row, $order->date_of_payment);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(21, $row, $order->re_costs);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(22, $row, $order->description);

                $row++;
            endforeach;

            $filename = 'bao_cao_bao_hiem.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có !'));
        }
    }

    function import($options = array()) {

        if (!empty($_FILES)) {

            //upload products excel file
            $config = array();
            $config['upload_path'] = './images/products-data/';
            $config['allowed_types'] = 'xls|xlsx|csv';
            $config['max_size'] = '5048';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $options['error'] = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();

                //read excel file
                $this->load->library('excel');
                $inputFileName = './images/products-data/' . $data['file_name'];

                //  Read your Excel workbook
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }

                //  Get worksheet dimensions
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++) {
                    //  Read a row of data into an array
                    $rowData = $sheet->ToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    //  Insert row data array into your database of choice here
                }

                foreach ($rowData as $index => $data) {
                    if ($index != 0) {
                        $id = $data[0];
                        $creator = $data[1];
                        $product_name = $data[2];
                        $created_date = $data[3];
                        $custom_buy_id = $data[4];
                        $custom_duoc_id = $data[5];
                        $origin_id = $data[6];
                        $code = $data[7];
                        $contract_date = $data[8];
                        $trademark_id = $data[9];
                        $categories_id = $data[10];
                        $price = $data[11];
                        $price_old = $data[12];
                        $commission = $data[13];
                        $price_real = $data[14];
                        $style_id = $data[15];
                        $date_of_payment = $data[16];
                        $re_costs = $data[17];
                        $description = $data[18];


                        $options['code'] = $code;

                        $products = $this->products_model->get_products($options);

                        //if productsname doesn't exist in system
                        if (count($products) == 0) {
                            //insert to products table
                            $products_data = array(
                                'creator' => $creator,
                                'product_name' => $product_name,
                                'created_date' => $created_date,
                                'custom_buy_id ' => $custom_buy_id,
                                'custom_duoc_id ' => $custom_duoc_id,
                                'origin_id' => $origin_id,
                                'code' => $code,
                                'contract_date' => $contract_date,
                                'trademark_id' => $trademark_id,
                                'categories_id' => $categories_id,
                                'price' => $price,
                                'price_old' => $price_old,
                                'commission' => $commission,
                                'price_real' => $price_real,
                                'style_id ' => $style_id,
                                'date_of_payment' => $date_of_payment,
                                're_costs' => $re_costs,
                                'description' => $description,
                            );
                            $position_add = $this->products_model->position_to_add_products(array('lang' => $data['lang']));

                            $products_data['position'] = $position_add;

                            $products_id = $this->products_model->insert($products_data);
                        } else {


                            $products_id = $products[0]->id;
                            foreach ($products as $value) {

                                $this->products_model->update(array(
                                    'id' => $value->id,
                                    'creator' => $creator,
                                    'product_name' => $product_name,
                                    'created_date' => $created_date,
                                    'custom_buy_id ' => $custom_buy_id,
                                    'custom_duoc_id ' => $custom_duoc_id,
                                    'origin_id ' => $origin_id,
                                    'code' => $code,
                                    'contract_date ' => $contract_date,
                                    'trademark_id ' => $trademark_id,
                                    'categories_id ' => $categories_id,
                                    'price ' => $price,
                                    'price_old' => $price_old,
                                    'commission  ' => $commission,
                                    'price_real' => $price_real,
                                    'style_id ' => $style_id,
                                    'date_of_payment' => $date_of_payment,
                                    're_costs' => $re_costs,
                                    'description ' => $description,
                                ));
                            }
                        }
                    }
                }
                $options['succeed'] = 'Bạn đã nhập dữ liệu thành công';
            }

            $this->browse();
        } else {
            $options['error'] = validation_errors();
            $options['header'] = 'Nhập bảo hiểm bằng excel';

            if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
                $options['options'] = $options;

            // Chuan bi du lieu chinh de hien thi
            $this->_view_data['main_content'] = $this->load->view('admin/products/add_product_form_excel', $options, TRUE);
            // Chuan bi cac the META
            $this->_view_data['title'] = 'Nhập bảo hiểm bằng excel' . DEFAULT_TITLE_SUFFIX;
            // Tra lai view du lieu cho nguoi su dung
            $this->load->view($this->_layout, $this->_view_data);
        }
        return FALSE;
//        else
//        {
//            $options['error'] = 'Bạn vui lòng chọn file để upload';
//        }
//        $options['page'] = 1;
//        return $this->browse($options);
    }

    private function _get_scripts() {
//        $scripts = '<script type="text/javascript" src="/plugins/tinymce/tinymce.min.js?v=4.1.7"></script>';
//        $scripts .= '<link rel="stylesheet" type="text/css" href="/plugins/fancybox/source/jquery.fancybox.css" media="screen" />';
//        $scripts .= '<script type="text/javascript" src="/plugins/fancybox/source/jquery.fancybox.pack.js"></script>';
//        $scripts .= '<script type="text/javascript">$(".iframe-btn").fancybox({"width":900,"height":500,"type":"iframe","autoScale":false});</script>';
//        $scripts .= '<style type=text/css>.fancybox-inner {height:500px !important;}</style>';
//        $scripts .= '<script type="text/javascript">enable_tiny_mce();</script>';
        return $scripts;
    }

    function get_custom_duoc_bh() {
        $options = array();
        $options['cmt_custom_duoc'] = $this->input->post('search_custom_duoc');
        $options['lang'] = 'vi';

        $view_data = array();

        $customers = $this->products_model->get_products($options);
//             $bh_cl     = $this->products_model->get_bh_con_lai($options);

        if (!empty($customers)) {
            foreach ($customers as $x => $item) {

                $title = '<strong >' . $options['cmt_custom_duoc'] . '</strong>';
                $location = '<strong >' . $options['cmt_custom_duoc'] . '</strong>';
                $title_st = str_ireplace($options['cmt_custom_duoc'], $title, $item->cmt_custom_duoc);
//                $loca_st = str_ireplace($options['cmt_custom_duoc'], $location, $item->custom_duoc_name);
                $name_cus = $item->custom_duoc_name;
                $gh_tv_tn = $item->tu_vong_tai_nan;
                $tt_bp = $item->tt_bp;
                ?>

                <div class="show" align="left">
                    <span class="custom_name_duoc"><?php echo $title_st; ?></span> | Tên khách hàng: 
                    <span class="id_cus_duoc"><?php echo $name_cus; ?></span>
                    <div class="goi_bao_hiem" style="display: none;">
                        <p>Gói quyền lợi của khách hàng</p>
                        <table style="font-size: 12px; border: 0px none; border-collapse: collapse; margin: 0px 0px 10px; padding: 0px; outline: none 0px; vertical-align: baseline; border-spacing: 0px; width: 555px; color: #585858; font-family: Arial, Helvetica, sans-serif; line-height: 18px; background-image: none; background-attachment: scroll; background-color: #f1f1f1; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0% 0%; background-repeat: repeat;">
                            <tbody style="border: 0px; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;"><th style="font-family: MyriadPro, arial; padding: 8px; border-width: 1px 1px 0px; margin: 0px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-top-color: #acacac; border-right-color: #acacac; border-left-color: #acacac; outline: 0px; vertical-align: baseline; color: #ffffff; text-transform: uppercase; background: #acacac;" colspan="2"><label><input type="radio" class="gh_tv_tn" name="loai_benh" value="gh_tv_tn"> TỬ VONG, TTTBVV do tai nạn</label></th></tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Số tiền bảo hiểm</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;"><span id="pri_tv_tn"><?php echo $gh_tv_tn; ?></span> đ</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Thương tật bộ phận</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;"><span id="per_tt_bp"><?php echo $tt_bp; ?></span>%</td>
                                </tr>

                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;"><th style="font-family: MyriadPro, arial; padding: 8px; border-width: 1px 1px 0px; margin: 0px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-top-color: #acacac; border-right-color: #acacac; border-left-color: #acacac; outline: 0px; vertical-align: baseline; color: #ffffff; text-transform: uppercase; background: #acacac;" colspan="2"><label><input type="radio" class="" name="loai_benh" value="luong_nn"> Trợ cấp lương ngày nghỉ</label></th></tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Tay l&aacute;i</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Spectra</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">P&ocirc;-tăng</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Promax chỉnh g&oacute;c</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Cốt y&ecirc;n</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Spectra</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Y&ecirc;n xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Velo</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">B&agrave;n đạp</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Thường</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;"><th style="font-family: MyriadPro, arial; padding: 8px; border-width: 1px 1px 0px; margin: 0px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-top-color: #acacac; border-right-color: #acacac; border-left-color: #acacac; outline: 0px; vertical-align: baseline; color: #ffffff; text-transform: uppercase; background: #acacac;" colspan="2">TỬ VONG, TTTBVV do ốm bệnh</th></tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Tay đề</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Shimano Sora 9sp</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Bộ đề sau</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Shimano Shimano Sora 9sp</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Bộ động cơ</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Panasonic 250w</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Phanh xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Shimano đĩa dầu thủy lực M396</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Đ&ugrave;i đĩa&nbsp;</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">FSA Gamma Drive Mega EXO 38/24T 170mm/175mm</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">L&iacute;p xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Deore 10sp 11/36T</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">X&iacute;ch xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Shimano 10s</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Ổ trục giữa</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">FSA Mega EXO BB-1000</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;"><th style="font-family: MyriadPro, arial; padding: 8px; border-width: 1px 1px 0px; margin: 0px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-top-color: #acacac; border-right-color: #acacac; border-left-color: #acacac; outline: 0px; vertical-align: baseline; color: #ffffff; text-transform: uppercase; background: #acacac;" colspan="2">ĐIỀU TRỊ NỘI TRÚ DO ỐM BÊNH</th></tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">V&agrave;nh xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">28 inch</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Moay-ơ</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Sun Ringle</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Nan hoa</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Th&eacute;p kh&ocirc;ng rỉ&nbsp;</td>
                                </tr>
                                <tr style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Lốp xe</td>
                                    <td style="font-family: Tahoma; padding: 5px; border-style: solid; border-color: #dddddd; margin: 0px; outline: 0px; vertical-align: baseline; border-collapse: collapse; background: transparent;">Spectra Duramax</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 	



                <?php
            }
        } else {
            ?>
            <div class="show" align="left">
                Rất tiêc. Chúng tôi không tìm thấy khách hàng nào              
            </div>
            <?php
        }
    }

    function check_bao_hiem() {
        $options = array();
        //so tien yeu cau
        $price_bt = $this->input->post('price_bt');
        // so ngay yeu cau
        $day_bt = $this->input->post('day_bt');
        // so tien tong bảo hiểm của gói

        $options['loai_benh'] = $this->input->post('loai_benh');
        $options['cmt_custom'] = $this->input->post('cmt_custom');
        $pri_tv_tn = $this->input->post('pri_tv_tn');
        $pri_tv_tn_dong = $pri_tv_tn != 0 ? get_price_in_vnd($pri_tv_tn) . ' đ' : get_price_in_vnd($pri_tv_tn);
        $per_tt_bp = $this->input->post('per_tt_bp');

        $view_data = array();

        $customers = $this->products_model->get_bh_con_lai($options);

        if (!empty($customers)) {
            $st_dbt = $customers->st_dbt;
            $st_dbt_dong = $st_dbt != 0 ? get_price_in_vnd($st_dbt) . ' đ' : get_price_in_vnd($st_dbt);
            $stbh_cl = $customers->stbh_cl;
            $stbh_cl_dong = $stbh_cl != 0 ? get_price_in_vnd($stbh_cl) . ' đ' : get_price_in_vnd($stbh_cl);
            $thong_qua = $stbh_cl - $price_bt;
            $thong_qua_dong = $thong_qua != 0 ? get_price_in_vnd($thong_qua) . ' đ' : get_price_in_vnd($thong_qua);
            if ($thong_qua >= 0) {
                echo '<p class="bg-primary">Giá trị giới hạn gói bảo hiểm không vượt quá: ' . $pri_tv_tn_dong . '</p>'
                . '<p class="bg-success">Đã sử dụng:' . $st_dbt_dong . '</p>'
                . '<p class="bg-info">Còn lại trước khi yêu cầu bồi thường: ' . $stbh_cl_dong . '</p>'
                . '<p class="bg-warning">Ước tính số tiền còn lại nếu áp dụng số tiền yêu cầu bồi thường: ' . $thong_qua_dong . '</p>'
                . '<p class="bg-danger">Số tiền yêu cầu bảo hiểm thỏa mãn điều kiện giới hạn. </p>';
            } else {
                $am_price = ($thong_qua * -1);
                $price = $am_price != 0 ? get_price_in_vnd($am_price) . ' đ' : get_price_in_vnd($am_price);
                echo '<p class="bg-primary">Giá trị giới hạn gói bảo hiểm không vượt quá: ' . $pri_tv_tn_dong . '</p>'
                . '<p class="bg-success">Đã sử dụng:' . $st_dbt_dong . '</p>'
                . '<p class="bg-info">Còn lại trước khi yêu cầu bồi thường: ' . $stbh_cl_dong . '</p>'
                . '<p class="bg-warning">Ước tính số tiền còn lại nếu áp dụng số tiền yêu cầu bồi thường: - ' . $price . '</p>'
                . '<p class="bg-danger">Số tiền yêu cầu bảo hiểm không được chấp thuận vì vượt quá số tiền giới hạn được bảo hiểm còn lại là: ' . $price . '</p>';

//                      echo '<p class="bg-danger">Số tiền yêu cầu bảo hiểm không được chấp thuận vì vượt quá số tiền được bảo hiểm còn lại là: '.$price.'</p>';
            }
            // kiem tra xem duoc boi thuong hay khong
        } else {
            $thong_qua = $pri_tv_tn - $price_bt;
            if ($thong_qua >= 0) {

                echo '<p class="bg-success">Số tiền yêu cầu bảo hiểm thỏa mãn điều kiện giới hạn.</p>';
            } else {
                $am_price = ($thong_qua * -1);
                $price = $am_price != 0 ? get_price_in_vnd($am_price) . ' đ' : get_price_in_vnd($am_price);
                echo '<p class="bg-danger">Số tiền yêu cầu bảo hiểm không được chấp thuận vì vượt quá số tiền được bảo hiểm còn lại là: ' . $price . '</p>';
            }
        }
    }

}
?>
