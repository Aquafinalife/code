<div class="box">
    <p style="text-align: center; margin-bottom: 30px; margin-top: 30px;">
        <img src="/images/process-finish.png" border="0" style="max-width: 100%;" />
    </p>
    <h1 class="title">Đặt hàng thành công</h1>
    <div class="clearfix"></div>
    <div class="alert alert-success">
        <?php
        $config = get_cache('configurations_' . get_language());
        if (!empty($config['success_order'])) {
            echo $config['success_order'];
        }
        ?>
    </div>
</div>