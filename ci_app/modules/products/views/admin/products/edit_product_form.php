<?php
if (isset($products) && is_object($products)) {
    $product_id = $products->id;
    $buy_name= $products->buy_name;
    $congty_canhan = $products->congty_canhan;
    $custom_buy_id = $products->custom_buy_id;
    $custom_duoc_name = $products->custom_duoc_name;
    $custom_duoc_id = $products->custom_duoc_id;
    $cmt_custom_duoc = $products->cmt_custom_duoc;
//    $price = $products->price;
//    $description = $products->description;
//    $date_of_payment = $products->date_of_payment;
//    $commission = $products->commission;
//    $contract_date = $products->contract_date;
//    $code = $products->code;
    //end
    $dn_bbh = $products->dn_bbh;
    $cn_bbh = $products->cn_bbh;
    $goi_bh = $products->goi_bh;
    $sohopdong = $products->sohopdong;
    $ngay_hieuluc = $products->ngay_hieuluc;
    $hoahong = $products->hoahong;
    $phi_bbh = $products->phi_bbh;
    $ngay_thanhtoan = $products->ngay_thanhtoan;
    $ghichu = $products->ghichu;
}

echo form_open_multipart($submit_uri);
if (isset($product_id))
    echo form_hidden('id', $product_id);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_cat');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thay đổi thông tin về bảo hiểm"</small>
    <span class="fright">
        <a class="button close" href="<?php echo PRODUCTS_ADMIN_BASE_URL; ?>"><em>&nbsp;</em>Đóng</a>
    </span>
    <br class="clear"/>
</div>

<div id="sort_success">Vị trí ảnh đã được cập nhật</div>
<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="nav nav-tabs tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Thêm đơn bảo hiểm</a></li>
        <!--        <li><a href="#tab2" data-toggle="tab">Thông tin người mua bảo hiểm</a></li>
                <li><a href="#tab3" data-toggle="tab">Thông tin người được hưởng bảo hiểm</a></li>
                <li><a href="#tab4" data-toggle="tab">Thông tin công ty bảo hiểm</a></li>
                <li><a href="#tab5" data-toggle="tab">Thông tin thanh toán</a></li>
                <li><a href="#tab6" data-toggle="tab">Ghi chú</a></li>-->
    </ul>
    <div id="tabs" class="tab-content tab_container">
        <div id="tab1" class="tab_content">
            <!--            <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr><td class="title">Tên cộng tác viên nếu có: </td></tr>
                                    <tr><td style="position:relative"><?php // echo form_input(array('name' => 'product_name', 'id' => 'searchid_ctv', 'class' => 'search_custom_ctv text-field', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;',  'value' => isset($product_name) ? $product_name : set_value('product_name')));       ?>
                                        <div id="result_ctv">
                                        </div>
                                        </td></tr>
                                </table>   
                            </div>
                           
                        </div>-->
            <div class="row">
                <div class="col-md-6">
                    <table>
                        <tr class="display_status_block"><td class="title">Ngôn ngữ: (<span>*</span>)</td></tr>
                        <tr class="display_status_block"><td><?php if (isset($lang_combobox)) echo $lang_combobox; ?></td></tr>

                        <tr><td class="title">Người mua: (<span>*</span>)</td></tr>
                        <tr><td><i style="font-size: 13px;">(Gõ tên người mua nếu khách hàng đã tồn tại hoặc bấm vào thêm mới khách hàng)</i></td></tr>
                        <tr>
                            <td style="position:relative">
                                <?php echo form_input(array('name' => 'buy_name', 'id' => 'searchid', 'class' => 'search_custom text-field', 'autocomplete' => 'off', 'style' => 'width:560px;', 'value' => isset($buy_name) ? $buy_name : set_value('buy_name'))); ?>
                                <?php echo form_input(array('name' => 'custom_buy_id', 'id' => 'id_custom', 'class' => 'text-field', 'autocomplete' => 'off', 'style' => 'width:560px;display: none;', 'value' => isset($custom_buy_id) ? $custom_buy_id : set_value('custom_buy_id'))); ?>
                                <div id="result">
                                </div>

                                <!--                        <div class="adropdown" style="">
                                                            <table style="width: 500px; margin: 46px 1px 0px; display: block;" cellspacing="0">
                                                                <caption style="display: none;">No Records Found</caption>
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Tên</th>
                                                                        <th>Số điện thoại</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="">
                                                                        <td style="display: block;">21</td>
                                                                        <td>Boli<span class="">v</span>ia</td>
                                                                        <td>BOL</td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="display: block;">22</td>
                                                                        <td>Bosnia and Herzego<span class="">v</span>ina</td>
                                                                        <td>BOL</td>
                                                                        
                                                                    </tr>
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>-->

                            </td>

                        </tr>

                    </table>
                </div>
                <div class="col-md-6">
                    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" style="margin-top: 56px;">Thêm khách hàng</button>-->
                </div>
            </div>
            <table>
                <tr><td class="title">Tên người được bảo hiểm: (<span>*</span>)</td></tr>
                <tr><td><i style="font-size: 13px;">(Gõ tên người được bảo hiểm)</i></td></tr>
                <tr>
                    <td >
                        <?php echo form_input(array('name' => 'custom_duoc_name', 'id' => 'searchid_duoc', 'class' => 'search_custom_duoc text-field', 'autocomplete' => 'off', 'style' => 'width:560px;', 'value' => isset($custom_duoc_name) ? $custom_duoc_name : set_value('custom_duoc_name'))); ?>
                        <?php echo form_input(array('name' => 'custom_duoc_id', 'id' => 'id_custom_duoc', 'class' => 'text-field', 'style' => 'width:560px;display: none;', 'value' => isset($custom_duoc_id) ? $custom_duoc_id : set_value('custom_duoc_id'))); ?>
                        <?php echo form_input(array('name' => 'cmt_custom_duoc', 'id' => 'cmt_custom_duoc', 'class' => 'text-field', 'style' => 'width:560px;display: none;', 'value' => isset($cmt_custom_duoc) ? $cmt_custom_duoc : set_value('cmt_custom_duoc'))); ?>
                        <?php
                        // if ($custom_duoc_id) {
//                        echo $custom_duoc_id;
//                    } 
                        ?>
                        <div id="result_duoc">
                        </div>
                    </td>
                </tr>

<!--                <tr><td class="title">Gói bảo hiểm: (<span>*</span>)</td></tr>
                <tr>
                    <td id="">
                <?php
//                if ($products_size) {
//                    echo $products_size;
//                }
                ?>
                    </td>
                </tr>-->

                <tr><td class="title">Công ty Bảo Hiểm: (<span>*</span>)</td></tr>
                <tr>
                    <td id="">
                        <select  name="dn_bbh" class="filter_dn_bbh" goi_bh="1">
                            <option value="">--Chọn Công ty/doanh nghiệp BH--</option>
                            <?php
                            if (!empty($a_dn_bbh)) {
                                foreach ($a_dn_bbh as $index) {
                                    $name = $index->name;
                                    $parent_id = $index->parent_id;
                                    $id = $index->id;
                                    $selected = ($id == $dn_bbh) ? 'selected="selected"' : '';
                                    if ($parent_id == FALSE) {
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <!--<tr><td class="title">Chi nhánh/phòng ban: (<span>*</span>)</td></tr>-->
<!--                <tr>
                    <td class="">
                        <div class="box_filter_cn_bbh">
                            <select name="cn_bbh" class="">
                                <option value="">--Chọn chi nhánh/phòng ban BH--</option>
                                <?php
//                                if ($product_id > 0) {
//                                    if (!empty($a_dn_bbh)) {
//                                        foreach ($a_dn_bbh as $index) {
//                                            $name = $index->name;
//                                            $parent_id = $index->parent_id;
//                                            $id = $index->id;
//                                            $selected = ($id == $cn_bbh) ? 'selected="selected"' : '';
//                                            if ($dn_bbh == $parent_id) {
//                                                ?>
                                                <option //<?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                                //<?php
//                                            }
//                                        }
//                                    }
//                                }
                                ?>
                            </select>
                        </div>

                    </td>
                </tr>-->

                <tr><td class="title">Chọn gói BH: (<span>*</span>)</td></tr>
                <tr>
                    <td class="">
                        <div class="box_filter_goi_bh">
                            <select name="goi_bh" class="">
                                <option value="">--Chọn gói BH--</option>
                                <?php
                                if ($product_id > 0) {
                                    if (!empty($a_goi_bh)) {
                                        foreach ($a_goi_bh as $index) {
                                            $name = $index->name;
                                            $id = $index->id;
                                            $selected = ($id == $goi_bh) ? 'selected="selected"' : '';
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </td>
                </tr>


<!--                <tr><td class="title">Nguồn khách hàng: (<span>*</span>)</td></tr>
                <tr><td id="products_origin"><?php
//                            if ($products_origin) {
//                                echo $products_origin;
//                            }
                ?></td></tr>-->


            </table>
            <table>

                <tr><td class="title">Số hợp đồng bảo hiểm: </td></tr>
                <tr><td><?php echo form_input(array('name' => 'sohopdong', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($sohopdong) ? $sohopdong : set_value('sohopdong'))); ?></td></tr>
                <tr><td class="title">Ngày hiệu lực: </td></tr>
                <tr><td><?php echo form_input(array('id' => 'news_created_date', 'name' => 'ngay_hieuluc', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_hieuluc) ? $ngay_hieuluc : set_value('ngay_hieuluc'))); ?></td></tr>

                <!--<tr><td class="title">Nhà cung cấp: (<span>*</span>)</td></tr>-->
<!--                <tr>
                    <td>
                <?php
//                        if ($products_trademark) {
////                            echo $products_trademark;
//                        }
                ?>
                    </td>
                </tr>-->

<!--                <tr><td class="title">Loại hình bảo hiểm: (<span>*</span>)</td></tr>-->
<!--                <tr>
                    <td class="dynamic_jq_a">
                        <select name="categories_combobox" class="btn dynamic_jq" id="category">
                            <option value="">Chọn nhà cung cấp đầu tiên</option>
                        </select>
                <?php
                if ($categories) {
//                    echo $categories;
                }
                ?>
                    </td>
                </tr>-->

                <tr><td class="title">Hoa hồng: </td></tr>
                <tr><td><i style="font-size: 13px;">(Tuân theo chính sách giảm phí của cty)</i></td></tr>
                <tr><td><?php echo form_input(array('name' => 'hoahong', 'class' => 'data_hh', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($hoahong) ? $hoahong : set_value('hoahong'))); ?></td></tr>


            </table>
            <table>
                <tr><td class="title">Phí của cty bảo hiểm: </td></tr>
                <tr><td>
                        <?php echo form_input(array('name' => 'phi_bbh', 'size' => '30', 'maxlength' => '10', 'class' => 'number', 'style' => 'width:560px;', 'value' => isset($phi_bbh) ? $phi_bbh : set_value('phi_bbh'))); ?>
                        <?php // echo $unit;   ?>
                        <?php // echo ' / ' . $product_unit;   ?>
                    </td></tr>

                <!--<tr><td class="title">Phí phải thu KH: </td></tr>-->
<!--                <tr><td>
                <?php // echo form_input(array('name' => 'price_old', 'size' => '30', 'maxlength' => '10', 'class' => 'number pri_required','style' => 'width:560px;', 'value' => $price_old));   ?>
                <?php // echo $unit_old;   ?>
                <?php // echo ' / ' . $product_unit_old;   ?>
                    </td></tr>-->

<!--<tr><td class="title">Số tiền thực thu: </td></tr>-->
<!--                <tr><td>
                <?php // echo form_input(array('name' => 'price_real', 'size' => '30', 'maxlength' => '10', 'class' => 'number pri_real','style' => 'width:560px;', 'value' => $price_real));   ?>
                <?php // echo $unit_real;   ?>
      
    </td></tr>-->
                <!--<tr><td class="title">Hình thức thanh toán: (<span>*</span>)</td></tr>-->
<!--                <tr><td id="products_style"><?php
//                        if ($products_style) {
//                            echo $products_style;
//                        }
                ?></td></tr>-->
                <tr><td class="title">Ngày thanh toán: (<span>*</span>)</td></tr>
                <tr><td><?php echo form_input(array('id' => 'date_of_payment', 'name' => 'ngay_thanhtoan', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_thanhtoan) ? $ngay_thanhtoan : set_value('ngay_thanhtoan'))); ?></td></tr>
                <!--<tr><td class="title">Còn lại: (<span>*</span>)</td></tr>-->
                <!--<tr><td><?php // echo form_input(array('name' => 're_costs','class'=> 'pri_cos', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;','value' => isset($re_costs) ? $re_costs : set_value('re_costs')));     ?></td></tr>-->

            </table>
            <table>
                <?php
//    $truong_phong = $this->phpsession->get('role_id');
//    if($truong_phong ==4 || $truong_phong ==1){
//    
                ?>
            <!--<tr><td class="title">Tình trạng bảo hiểm: </td></tr>-->
               <!--<tr><td id="category">-->
                <?php
                // if ($products_state) {
//                    echo $products_state;
//                } 
                ?>
                <!--</td></tr>-->
                <?php // } ?>



                <tr><td class="title">Ghi chú bảo hiểm: </td></tr>
                <tr><td><?php echo form_textarea(array('cols' => 65, 'rows' => 8, 'id' => 'content', 'name' => 'ghichu', 'value' => ($ghichu != '') ? $ghichu : set_value('ghichu'), 'class' => 'wysiwyg elm1')); ?></td></tr>
            </table>
            <br class="clear">
            <div style="margin-top: 10px;"></div>
            <?php
            $truong_phong = $this->phpsession->get('role_id');
            if ($truong_phong != 5) {
                ?>
                <input type="submit" name="btnSubmit" value="<?php if (isset($button_name)) echo $button_name; ?>" class="btn btn-danger" />
                <input type="reset" value="Làm lại" class="btn" />
            <?php } ?>
            <br class="clear">&nbsp;
        </div>

        <div id="tab2" class="tab_content">


            <a class="btn btn-success btnPrevious">Quay lại</a>
            <a class="btn btn-primary btnNext">Đi tiếp</a>

        </div>

        <div id="tab3" class="tab_content">

            <a class="btn btn-success btnPrevious">Quay lại</a>
            <a class="btn btn-primary btnNext">Đi tiếp</a>
        </div>
        <div id="tab4" class="tab_content">

            <a class="btn btn-success btnPrevious">Quay lại</a>
            <a class="btn btn-primary btnNext">Đi tiếp</a>
        </div>
        <div id="tab5" class="tab_content">

            <a class="btn btn-success btnPrevious">Quay lại</a>
            <a class="btn btn-primary btnNext">Đi tiếp</a>
        </div>
        <div id="tab6" class="tab_content">




        </div>

    </div>

</div>



<?php echo form_close(); ?>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php
            echo form_open($submit_uri_cus);
            if (isset($id))
                echo form_hidden('id', $id);
            $fullname = isset($fullname) ? $fullname : '';
            $email = isset($email) ? $email : '';
            $created_date = isset($created_date) ? $created_date : '';
            $submit_uri = isset($submit_uri) ? $submit_uri : '';
            echo form_hidden('is_add_edit_category', TRUE);
            echo form_hidden('form', 'customers_cat');
            ?>

            <div class="page_header">
                <h1 class="fleft">Thêm khách hàng mới</h1>
                <small class="fleft">"Chi tiết khách hàng"</small>
                <!--<span class="fright"><a class="button close" href="<?php // echo CUSTOM_ADMIN_BASE_URL;       ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>-->
                <br class="clear"/>
            </div>

            <div class="form_content">
                <?php $this->load->view('powercms/message'); ?>

                <ul class="tabs">
                    <li><a href="#tab1">Thông tin khách hàng</a></li>

                </ul>

                <table>

                    <tr><td class="title">Họ tên khách hàng:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'fullname', 'id' => 'fullname', 'class' => 'btn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $fullname)); ?></td>
                    </tr>
                    <tr><td class="title">Số điện thoại: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'phone', 'id' => 'phone', 'required' => 'required', 'class' => 'btn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $phone)); ?></td>
                    </tr>
                    <tr><td class="title">Email: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'email', 'id' => 'email', 'class' => 'btn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $email)); ?></td>
                    </tr>
                    <tr><td class="title">Ngày sinh:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'DOB', 'id' => 'DOB', 'class' => 'btn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $DOB)); ?></td>
                    </tr>
                    <tr><td class="title">Địa chỉ:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'address', 'id' => 'address', 'class' => 'btn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $address)); ?></td>
                    </tr>
                    <tr><td class="title">Ngày tạo: </td></tr>
                    <tr>
                        <td>
                            <?php echo form_input(array('name' => 'created_date', 'id' => 'created_date', 'class' => 'btn', 'readonly' => TRUE, 'size' => '50', 'maxlength' => '10', 'value' => $created_date)); ?>

                        </td>
                    </tr>
                </table>

                <br class="clear"/>
                <div style="margin-top: 10px;"></div>

                <a onclick="add_custom_quick();" title="Thêm khách hàng mới" class="btn  btn-primary contactquicksubmit pull-right" id="contact_quick_submit">Thêm khách hàng mới<span class="seemore_arrow_black"></span></a>
                <br class="clear"/>&nbsp;
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<style>
    .adropdown {
        background-color: #fff;
        margin: 0;
        padding: 0;
        display: block;
        /*    top: 0px;
            left: 0px;*/
        text-align: left;
        /*position: absolute;*/
        box-shadow: 0px 0px 20px rgba(0,0,0,0.10), 0px 10px 20px rgba(0,0,0,0.05), 0px 20px 20px rgba(0,0,0,0.05), 0px 30px 20px rgba(0,0,0,0.05);
    }
    .adropdown table {
        font-family: 'Arial';
        margin: 35px 1px 0px 1px;
        border-collapse: collapse;
        border-bottom: 2px solid #00cccc;
        border-spacing: 0px;
        display: none;
        color: #999;
        cursor: default;
        width: 100%;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
    }

    .adropdown tr:hover {
        background: #f4f4f4;
        color: #555;
    }

    .adropdown th {
        background: #00cccc;
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        padding: 5px 10px;
        text-align: center;
    }

    .adropdown tr td {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, sans-serif;
        margin: 0;
        font-variant: normal;
        font-size: 12px;
        padding: 5px;
        min-height: 15px;
        border-bottom: 1px solid #eee;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        text-align: left;
    }

    .adropdown .selected {
        background: #17D5D5;
        padding-bottom: 8px;
        color: #fff;
    }

    .adropdown div {
        position: absolute;
    }

    .adropdown table caption {
        caption-side: bottom;
        padding: 10px;
        display: none;
        font-size: 12px;
    }

</style>