<?php if(!empty($products)) { ?>
<div class="page-product-box">
    <h3 class="heading"><?php echo __('IP_products_similar'); ?></h3>
    <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
        <?php foreach($products as $key => $value){
        if(SLUG_ACTIVE==0){
            $uri = get_base_url() . url_title(trim($value->product_name), 'dash', TRUE) . '-ps' . $value->id;
        }else{
            $uri = get_base_url() . $value->slug;
        }
        $image = is_null($value->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/thumbnails/'.$value->image_name;
        $value_name = limit_text($value->product_name, 120);
//        $value_category = limit_text($value->category, 100);
        $price = $value->price > 0 ? get_price_in_vnd($value->price) . ' ₫' : get_price_in_vnd($value->price);    
        $price_old = $value->price_old > 0 ? get_price_in_vnd($value->price_old) . ' ₫' : 0;
        $price_count = $value->price_old - $value->price;
        $price_discount = $price_count > 0 ? get_price_in_vnd($price_count) . ' ₫' : 0;
        $saleoff = ($price_count/$value->price_old)*100;
        $saleoff = round($saleoff,0);
    ?>
        <li>
            <div class="product-container">
                <div class="left-block">
                    <a href="<?php echo $uri; ?>">
                        <img class="img-responsive" alt="<?php echo $value->product_name; ?>" title="<?php echo $value->product_name; ?>" src="<?php echo $image; ?>" />
                    </a>
                    <div class="quick-view">
                            <a title="Add to my wishlist" class="heart" href="#"></a>
                            <a title="Add to compare" class="compare" href="#"></a>
                            <a title="Quick view" class="search" href="#"></a>
                    </div>
                    <div class="add-to-cart">
                        <a title="Add to Cart" class="fast_addtocart" onclick="fast_addtocart('<?php echo $value->id;?>');" >Mua ngay</a>
                    </div>
                </div>
                <div class="right-block">
                    <h5 class="product-name"><a href="<?php echo $uri; ?>"><?php echo $value->product_name; ?></a></h5>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                    <div class="content_price">
                        <span class="price product-price"><?php echo $price; ?></span>
                        <?php if(!empty($price_old) && $price_old <> 0){ ?>
                        <span class="price old-price"><?php echo $price_old; ?></span>
                        <?php } ?>
                       
                    </div>
                </div>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>

<?php } ?>