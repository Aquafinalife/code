<?php

class Customers_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_canhan_dbh() {
        $this->db->select('*');
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.custom_duoc_id = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }
    //end
    function get_doanhnghiep_mbh($options= array()) {
        $this->db->select('*');
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.custom_buy_id = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        // WHERE
         if (isset($options['phanloai_kh']) && !empty($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS.'.phanloai_kh', $options['phanloai_kh']);
        }
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }

    public function get_customers($options = array()) {

        $this->db->select(' customers.*,
                        (select count(*) from ' . $this->db->dbprefix . 'orders where user_id =' . $this->db->dbprefix . 'customers.id) as count_order
                    ');

//        $this->db->join('customers_categories', 'customers_categories.id = customers.cat_id', 'left');

        if (isset($options['status'])) {
            $this->db->where('customers.status', $options['status']);
        }

        if (isset($options['cmt']) && !empty($options['cmt'])) {
            $this->db->where('customers.cmt', $options['cmt']);
        }

        if (isset($options['user_id']))
            $this->db->where('customers.id', $options['user_id']);

        if (isset($options['id']))
            $this->db->where('customers.id', $options['id']);

        if (isset($options['fullname'])) {
            $this->db->where('customers.fullname', $options['fullname']);
        }

        if (isset($options['phanloai_kh'])) {
            $this->db->where('customers.phanloai_kh', $options['phanloai_kh']);
        }
        if (isset($options['parent_id'])) {
            $this->db->where('customers.parent_id', $options['parent_id']);
        }
        if (isset($options['mst_cmt'])) {
            $this->db->where('customers.mst_cmt', $options['mst_cmt']);
        }


        if (isset($options['search']) && $options['search'] != '') {
            $where = "(" . $this->db->dbprefix . "customers.congty_canhan like'%" . $this->db->escape_str($options['search']) . "%'";
            $where .= " or " . $this->db->dbprefix . "customers.didong like'%" . $this->db->escape_str($options['search']) . "%')";
            $this->db->where($where);
        }

//        if (isset($options['search']) && $options['search'] != '') {
//            $where = "(" . $this->db->dbprefix . "customers.fullname like'%" . $this->db->escape_str($options['search']) . "%'";
//            $where .= " or " . $this->db->dbprefix . "customers.phone like'%" . $this->db->escape_str($options['search']) . "%')";
//            $this->db->where($where);
//        }


        if (isset($options['email'])) {
            $this->db->where('customers.email', $options['email']);
        }

        if (isset($options['password'])) {
            $this->db->where('customers.password', $options['password']);
        }

        // 
        if (isset($options['a_id'])) {
            $this->db->where_in('customers.id', $options['a_id']);
        }
        //end
        // Loc theo trang
        if (isset($options['limit']) && isset($options['offset']))
            $this->db->limit($options['limit'], $options['offset']);
        else if (isset($options['limit']))
            $this->db->limit($options['limit']);

        if (isset($options['sort_by_viewed'])) {
            $this->db->order_by('customers.viewed desc, customers.updated_date desc, customers.created_date desc');
        } elseif (isset($options['random']) && $options['random'] == TRUE) {
            $this->db->order_by('RAND()');
        } else {
            $this->db->order_by('customers.updated_date desc, customers.created_date desc');
        }

        $query = $this->db->get('customers');

        if (isset($options['last_row']))
            return $query->row(0);

        if ($query->num_rows() > 0) {
            if (isset($options['id'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if ((isset($options['onehit']) && $options['onehit'] == TRUE) || isset($options['get_one'])) {
                return $query->row(0);
            }
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_customers_buy_id($options = array()) {

        $this->db->select(' customers.*,
                        
                    ');

//        $this->db->join('customers_categories', 'customers_categories.id = customers.cat_id', 'left');



        if (isset($options['id']))
            $this->db->where('customers.id', $options['id']);

        if (isset($options['fullname'])) {
            $this->db->where('customers.fullname', $options['fullname']);
        }

        $query = $this->db->get('customers');

        if (isset($options['last_row']))
            return $query->row(0);

        if ($query->num_rows() > 0) {
            if (isset($options['id'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if (isset($options['onehit']) && $options['onehit'] == TRUE) {
                return $query->row(0);
            }
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_customers_count($options = array()) {
        return count($this->get_customers($options));
    }

    public function update_customers_view($id = 0) {
        $this->db->set('viewed', 'viewed + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('customers');
    }

    // check username khi đăng ký đã tồn tại chưa
    function is_available_username_customers($params = array()) {
        $this->db->where('email', $params['email']);

        $query = $this->db->get('customers');

        if (count($query->row()) > 0) {
            $this->_last_message = '<p>Email: <strong>' . $params['email'] . '</strong> đã tồn tại trong hệ thống.</p>';
            return FALSE;
        }
        return TRUE;
    }

    function get_last_message() {
        return $this->_last_message;
    }

    public function get_customers_buy_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'custom_buy_id';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('customers');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['fullname'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

    public function get_customers_duoc_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'custom_duoc_id';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('customers');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['fullname'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

}

?>