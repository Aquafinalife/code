<?php
$selected_df = 'selected="selected"';
//$a_customers_type = array(
//    CANHAN => 'Cá nhân',
//    DOANHNGHIEP => 'Doanh nghiệp',
//);
$a_customers_type = get_a_customers_type();
$a_customers_type1 = array(
    VIP => 'Vip',
    THUONG => 'Thường',
);
$a_customers_relationship = array(
    BANTHAN => 'Bản thân',
    NHANVIEN => 'Nhân viên',
    NGUOITHAN => 'Người thân',
);
$a_gender = array(
    NAM => 'Nam',
    NU => 'Nữ',
    KHAC => 'Khác',
);
$a_loai_bh = array(
    NMBH => 'Người mua bảo hiểm',
    NDBH => 'Người được bảo hiểm',
);

if (isset($customers)) {
    $id = $customers->id;
    $parent_id = $customers->parent_id;

    $phanloai_kh = $customers->phanloai_kh;
    $phanloai_bh = $customers->phanloai_bh;
    $hopdong_baohiem = $customers->hopdong_baohiem;
    $congty_canhan = $customers->congty_canhan;
    $ngaysinh = $customers->ngaysinh;
    $congtycon_chinhanh = $customers->congtycon_chinhanh;
    $phongban = $customers->phongban;
    $diachi = $customers->diachi;
    $mst_cmt = $customers->mst_cmt;
    $ngaycap = $customers->ngaycap;
    $noicap = $customers->noicap;
    $email = $customers->email;
    $dienthoai = $customers->dienthoai;
    $didong = $customers->didong;
    $phanloai_kh1 = $customers->phanloai_kh1;
    $gioitinh = $customers->gioitinh;
    $ndbh_quanhe = $customers->ndbh_quanhe;
    //
//    $ndbh_phanloai_kh = $customers->ndbh_phanloai_kh;
//    $ndbh_hopdong_baohiem = $customers->ndbh_hopdong_baohiem;
//    $ndbh_congty_canhan = $customers->ndbh_congty_canhan;
//    $ndbh_congtycon_chinhanh = $customers->ndbh_congtycon_chinhanh;
//    $ndbh_phongban = $customers->ndbh_phongban;
//    $ndbh_diachi = $customers->ndbh_diachi;
//    $ndbh_mst_cmt = $customers->ndbh_mst_cmt;
//    $ndbh_ngaycap = $customers->ndbh_ngaycap;
//    $ndbh_noicap = $customers->ndbh_noicap;
//    $ndbh_email = $customers->ndbh_email;
//    $ndbh_dienthoai = $customers->ndbh_dienthoai;
//    $ndbh_didong = $customers->ndbh_didong;
//    $ndbh_phanloai_kh1 = $customers->ndbh_phanloai_kh1;
//    $ndbh_quanhe = $customers->ndbh_quanhe;
}

$id = isset($id) ? $id : '0';
$parent_id = isset($parent_id) ? $parent_id : '0';
$phanloai_kh = isset($phanloai_kh) ? $phanloai_kh : '';
$hopdong_baohiem = isset($hopdong_baohiem) ? $hopdong_baohiem : '';
$congty_canhan = isset($congty_canhan) ? $congty_canhan : '';
$ngaysinh = isset($ngaysinh) ? $ngaysinh : '';
$congtycon_chinhanh = isset($congtycon_chinhanh) ? $congtycon_chinhanh : '';
$phongban = isset($phongban) ? $phongban : '';
$diachi = isset($diachi) ? $diachi : '';
$mst_cmt = isset($mst_cmt) ? $mst_cmt : '';
$ngaycap = isset($ngaycap) ? $ngaycap : '';
$noicap = isset($noicap) ? $noicap : '';
$email = isset($email) ? $email : '';
$dienthoai = isset($dienthoai) ? $dienthoai : '';
$didong = isset($didong) ? $didong : '';
$phanloai_kh1 = isset($phanloai_kh1) ? $phanloai_kh1 : '';
$gioitinh = isset($gioitinh) ? $gioitinh : '';
$phanloai_bh = isset($phanloai_bh) ? $phanloai_bh : '';
$ndbh_quanhe = isset($ndbh_quanhe) ? $ndbh_quanhe : '';
//
//$ndbh_phanloai_kh = isset($ndbh_phanloai_kh) ? $ndbh_phanloai_kh : '';
//$ndbh_hopdong_baohiem = isset($ndbh_hopdong_baohiem) ? $ndbh_hopdong_baohiem : '';
//$ndbh_congty_canhan = isset($ndbh_congty_canhan) ? $ndbh_congty_canhan : '';
//$ndbh_congtycon_chinhanh = isset($ndbh_congtycon_chinhanh) ? $ndbh_congtycon_chinhanh : '';
//$ndbh_phongban = isset($ndbh_phongban) ? $ndbh_phongban : '';
//$ndbh_diachi = isset($ndbh_diachi) ? $ndbh_diachi : '';
//$ndbh_mst_cmt = isset($ndbh_mst_cmt) ? $ndbh_mst_cmt : '';
//$ndbh_ngaycap = isset($ndbh_ngaycap) ? $ndbh_ngaycap : '';
//$ndbh_noicap = isset($ndbh_noicap) ? $ndbh_noicap : '';
//$ndbh_email = isset($ndbh_email) ? $ndbh_email : '';
//$ndbh_dienthoai = isset($ndbh_dienthoai) ? $ndbh_dienthoai : '';
//$ndbh_didong = isset($ndbh_didong) ? $ndbh_didong : '';
//$ndbh_phanloai_kh1 = isset($ndbh_phanloai_kh1) ? $ndbh_phanloai_kh1 : '';
//$ndbh_quanhe = isset($ndbh_quanhe) ? $ndbh_quanhe : '';


echo form_open($submit_uri);
echo form_hidden('id', $id);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'customers_cat');
?>
<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Chi tiết khách hàng"</small>
    <span class="fright"><a class="button close" href="<?php echo CUSTOM_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <ul class="tabs">
        <li><a href="#tab1">Thông tin bên mua bảo hiểm</a></li>
        <!--<li><a href="#tab2">Thông tin người được bảo hiểm</a></li>-->
    </ul>
    <?php $this->load->view('powercms/message'); ?>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Loại khách hàng:</td></tr>
                    <tr>
                        <td>
                            <select name="phanloai_kh" class="btn">
                                <?php
                                if (!empty($a_customers_type)) {
                                    foreach ($a_customers_type as $k => $v) {
                                        $selected = ($k == $phanloai_kh) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            <?php // echo form_input(array('name' => 'fullname', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $fullname));  ?>
                        </td>
                    </tr>
                    <tr><td class="title">Tên Công ty / Cá nhân:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'congty_canhan', 'size' => '50', 'style' => 'width:560px;', 'value' => $congty_canhan)); ?></td>
                    </tr> 
                    <tr><td class="title">Ngày sinh:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('class' => 'show_datepicker', 'name' => 'ngaysinh', 'size' => '50', 'style' => 'width:560px;', 'value' => $ngaysinh)); ?></td>
                    </tr> 
                    <tr><td class="title">Trực thuộc công ty:</td></tr>
                    <tr>
                        <td>
                            <select name="parent_id" class="btn">
                                <option value="0">Công ty/Cá nhân riêng</option>
                                <?php
                                if (isset($customers_b) && !empty($customers_b)) {
                                    foreach ($customers_b as $index) {
                                        $b_name = $index->congty_canhan;
                                        $b_id = $index->id;
                                        $b_parent_id = $index->parent_id;
                                        $selected = ($b_id === $parent_id) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $b_id ?>"><?php echo $b_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Loại hình bảo hiểm:</td></tr>
                    <tr>
                        <td>
                            <select name="phanloai_bh" class="btn">
                                <?php
                                if (!empty($a_loai_bh)) {
                                    foreach ($a_loai_bh as $k => $v) {
                                        $selected = ($k == $phanloai_bh) ? $selected_df : '';
                                        ?>
                                        <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Quan hệ:</td></tr>
                    <tr>
                        <td>
                            <select name="ndbh_quanhe" class="btn">
                                <?php
                                if (!empty($a_customers_relationship)) {
                                    foreach ($a_customers_relationship as $k => $v) {
                                        $selected = ($k == $ndbh_quanhe) ? $selected_df : '';
                                        ?>
                                        <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Hợp đồng bảo hiểm:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'hopdong_baohiem', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $hopdong_baohiem)); ?></td>
                    </tr>                    

<!--                    <tr><td class="title">Công ty con / Chi nhánh:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'congtycon_chinhanh', 'size' => '50', 'style' => 'width:560px;', 'value' => $congtycon_chinhanh)); ?></td>
                    </tr>-->

                    <tr><td class="title">Phòng ban:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'phongban', 'size' => '50', 'style' => 'width:560px;', 'value' => $phongban)); ?></td>
                    </tr>

                    <tr><td class="title">Địa chỉ:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'diachi', 'size' => '50', 'style' => 'width:560px;', 'value' => $diachi)); ?></td>
                    </tr>                    

                </table>
            </div>

            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">ĐKKD - MST / CMTND - Hộ Chiếu:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'mst_cmt', 'size' => '50', 'style' => 'width:560px;', 'value' => $mst_cmt)); ?></td>
                    </tr>
                    <tr><td class="title">Ngày cấp:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('class' => 'show_datepicker', 'name' => 'ngaycap', 'size' => '50', 'style' => 'width:560px;', 'value' => $ngaycap)); ?></td>
                    </tr>

                    <tr><td class="title">Nơi cấp:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'noicap', 'size' => '50', 'style' => 'width:560px;', 'value' => $noicap)); ?></td>
                    </tr>
                    <tr><td class="title">Email:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'email', 'size' => '50', 'style' => 'width:560px;', 'value' => $email)); ?></td>
                    </tr>

                    <tr><td class="title">Điện thoại:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dienthoai', 'size' => '50', 'style' => 'width:560px;', 'value' => $dienthoai)); ?></td>
                    </tr>

                    <tr><td class="title">Di động:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'didong', 'size' => '50', 'style' => 'width:560px;', 'value' => $didong)); ?></td>
                    </tr>

                    <tr><td class="title">Phân loại khách hàng:</td></tr>
                    <tr>
                        <td>
                            <select name="phanloai_kh1" class="btn">
                                <?php
                                if (!empty($a_customers_type1)) {
                                    foreach ($a_customers_type1 as $k => $v) {
                                        $selected = ($k == $phanloai_kh1) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr><td class="title">Giới tính:</td></tr>
                    <tr>
                        <td>
                            <select name="gioitinh" class="btn">
                                <?php
                                if (!empty($a_gender)) {
                                    foreach ($a_gender as $k => $v) {
                                        $selected = ($k == $gioitinh) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <!--        <div id="tab2" class="tab_content">
                    <div class="col-sm-6 col-xs-12">
                        <table>
                            <tr><td class="title">Loại khách hàng:</td></tr>
                            <tr>
                                <td>
                                    <select name="ndbh_phanloai_kh" class="btn">
        <?php
        if (!empty($a_customers_type)) {
            foreach ($a_customers_type as $k => $v) {
                $selected = ($k == $ndbh_phanloai_kh) ? $selected_df : '';
                ?>
                                                                <option value="<?php echo $k ?>"><?php echo $v ?></option>
                <?php
            }
        }
        ?>
                                    </select>
        
        <?php // echo form_input(array('name' => 'fullname', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $fullname));  ?>
                                </td>
                            </tr>
                            <tr><td class="title">Hợp đồng bảo hiểm:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_hopdong_baohiem', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $ndbh_hopdong_baohiem)); ?></td>
                            </tr>
        
                            <tr><td class="title">Tên Công ty / Cá nhân:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_congty_canhan', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_congty_canhan)); ?></td>
                            </tr>
        
                            <tr><td class="title">Công ty con / Chi nhánh:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_congtycon_chinhanh', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_congtycon_chinhanh)); ?></td>
                            </tr>
        
                            <tr><td class="title">Phòng ban:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_phongban', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_phongban)); ?></td>
                            </tr>
        
                            <tr><td class="title">Địa chỉ:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_diachi', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_diachi)); ?></td>
                            </tr>
        
                            <tr><td class="title">ĐKKD - MST / CMTND - Hộ Chiếu:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_mst_cmt', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_mst_cmt)); ?></td>
                            </tr>
                        </table>
                    </div>
        
                    <div class="col-sm-6 col-xs-12">
                        <table>
                            <tr><td class="title">Ngày cấp:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('class' => 'show_datepicker', 'name' => 'ndbh_ngaycap', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_ngaycap)); ?></td>
                            </tr>
        
                            <tr><td class="title">Nơi cấp:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_noicap', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_noicap)); ?></td>
                            </tr>
        
                            <tr><td class="title">Email:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_email', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_email)); ?></td>
                            </tr>
        
                            <tr><td class="title">Điện thoại:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_dienthoai', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_dienthoai)); ?></td>
                            </tr>
        
                            <tr><td class="title">Di động:</td></tr>
                            <tr>
                                <td><?php echo form_input(array('name' => 'ndbh_didong', 'size' => '50', 'style' => 'width:560px;', 'value' => $ndbh_didong)); ?></td>
                            </tr>
        
                            <tr><td class="title">Phân loại khách hàng:</td></tr>
                            <tr>
                                <td>
                                    <select name="ndbh_phanloai_kh1" class="btn">
        <?php
        if (!empty($a_customers_type1)) {
            foreach ($a_customers_type as $k => $v) {
                $selected = ($k == $ndbh_phanloai_kh1) ? $selected_df : '';
                ?>
                                                                <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                <?php
            }
        }
        ?>
                                    </select>
                                </td>
                            </tr>
        
                            <tr><td class="title">Quan hệ:</td></tr>
                            <tr>
                                <td>
                                    <select name="ndbh_quanhe" class="btn">
        <?php
        if (!empty($a_customers_relationship)) {
            foreach ($a_customers_relationship as $k => $v) {
                $selected = ($k == $ndbh_quanhe) ? $selected_df : '';
                ?>
                                                                <option value="<?php echo $k ?>"><?php echo $v ?></option>
                <?php
            }
        }
        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
        
                </div>-->

    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>

    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>