<?php

class Customers_Admin extends MY_Controller {

    /**
     * Chuan bi cac bien co ban
     */
    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login', $this->router->fetch_module());
        // Khoi tao cac bien
        $this->_layout = 'admin_ui/layout/main';
        // Chuan bi link cho viec phan trang
        $this->_view_data['url'] = CUSTOM_ADMIN_BASE_URL;
        //$this->output->enable_profiler(TRUE);
        $this->load->model('products/products_model');
    }
    
    function ad_get_reports() {
        $data = array();

        $phanloai_kh = isset($_GET['phanloai_kh']) ? $_GET['phanloai_kh'] : '';
        // Nếu kh là doanh nghiệp
        if ($phanloai_kh == DOANHNGHIEP) {
            $tpl= 'admin/reports/reports_list_dn';
            // Lấy doanh nghiệp 
            $customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP));
            if (!empty($customers_b)) {
                $option_dn = '';
                foreach ($customers_b as $index) {
                    $id = $index->id;
                    $parent_id = $index->parent_id;
                    $congty_canhan = $index->congty_canhan;
                    $selected = ($_GET['dn'] == $id) ? 'selected="selected"' : '';
                    if ($parent_id == FALSE) {
                        $option_dn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                    }
                }
                $data['selectbox_dn'] = '<select type="2" selectbox_name="dn" name="dn" class="js-example-disabled-results" id="filter_doanhnghiep">
                            <option value="">Chọn doanh nghiệp mua BH</option>
                            ' . $option_dn . '
                        </select>';
                // Nếu tìm theo 1 doanh nghiệp
                if ((isset($_GET['cn']) && $_GET['cn'] != '') || (isset($_GET['dn']) && $_GET['dn'] != '')) {
                    if (isset($_GET['dn']) && $_GET['dn'] != '') {
                        $dn_id = $_GET['dn'];
                    }

                    if (!empty($customers_b)) {
                        $option_cn = '';
                        foreach ($customers_b as $index) {
                            $id = $index->id;
                            $congty_canhan = $index->congty_canhan;
                            $parent_id = $index->parent_id;
                            $selected = ($_GET['cn'] == $id) ? 'selected="selected"' : '';
                            if ($dn_id == $parent_id) {
                                $option_cn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                            }
                        }
                        $data['selectbox_cn'] = '<select type="2" selectbox_name="cn" name="cn" class="js-example-disabled-results" id="filter_chinhanh">
                            <option value="">Chọn công ty con/Chi nhánh BH</option>
                            ' . $option_cn . '
                        </select>';
                    }
                }
            }
            // Nếu tìm theo 1 doanh nghiệp
            $dn_id = isset($_GET['dn']) ? $_GET['dn'] : '';
            if ($dn_id > 0 && $dn_id != '') {
                $cn_id = isset($_GET['cn']) ? $_GET['cn'] : '';
                // Nếu tìm theo 1 chi nhánh
                if ($cn_id > 0) {
                    $dn_id = $cn_id;
                }
                // Lấy thông tin doanh nghiệp
                $data_get = array(
                    'id' => $dn_id,
                    'get_one' => TRUE,
                    'skip_join' => TRUE,
                );
                $dn_mbh = $this->customers_model->get_customers($data_get);

                if (is_object($dn_mbh)) {
                    $data['dn_mbh'] = $data['kh_mbh'] = $dn_mbh;
                    // Lấy id khách hàng được mua BH bới dn này
                    $data_get = array(
                        'custom_buy_id' => $dn_id,
                    );

                    $products = $this->products_model->get_products_by_options($data_get);

                    if (!empty($products)) {
                        $data['products'] = $products;
                        $str_goi_id = $str_kh_dbh_id = '';
                        foreach ($products as $index) {
                            $goi_id = $index->size;
                            $kh_dbh_id = $index->custom_duoc_id;
                            $str_goi_id .= $goi_id . ',';
                            $str_kh_dbh_id .= $kh_dbh_id . ',';
                        }

                        $str_goi_id .= 0;
                        $str_kh_dbh_id .= 0;
                        $a_goi_id = array_unique(@explode(',', $str_goi_id));
                        $a_kh_dbh_id = array_unique(@explode(',', $str_kh_dbh_id));

                        // Lấy thông tin khách hàng được bh
                        $data_get = array(
                            'a_id' => $a_kh_dbh_id,
                        );
                        $kh_dbh = $this->customers_model->get_customers($data_get);
                        if (!empty($kh_dbh)) {
                            $data['kh_dbh'] = $kh_dbh;
                        }
                        // Lấy thông tin các gói
                        $data_get = array(
                            'a_id' => $a_goi_id,
                        );
                        $goi_bh = $this->products_model->get_products_size_by_options($data_get);
                        if (!empty($goi_bh)) {
                            $data['goi_bh'] = $goi_bh;
                        }
                    }
                }
            }
        } else {
            
            $tpl= 'admin/reports/reports_list_cn';
            $key = isset($_GET['key']) ? $_GET['key'] : '';
            if ($key != '') {
                // Lấy kh được bh theo cmt/hộ chiếu
                $data_get = array(
                    'mst_cmt' => trim($key),
                    'get_one' => TRUE,
                    'skip_join' => TRUE,
                );
                $kh_dbh = $this->customers_model->get_customers($data_get);
                
                if (is_object($kh_dbh)) {
                    $data['kh_dbh'] = $kh_dbh;
                    $kh_dbh_id = $kh_dbh->id;

                    // Lấy thông tin bảo hiểm
                    $data_get = array(
                        'custom_duoc_id' => $kh_dbh_id,
                        'get_one' => TRUE,
                    );
                    $products = $this->products_model->get_products_by_options($data_get);
                    
                    if (is_object($products)) {
                        $kh_mbh_id = $products->custom_buy_id;
                        $goi_id = $products->size;

                        // Lấy thông tin kh mua bh
                        $data_get = array(
                            'id' => $kh_mbh_id,
                            'get_one' => TRUE,
                            'skip_join' => TRUE,
                        );
                        $kh_mbh = $this->customers_model->get_customers($data_get);
                        if (is_object($kh_mbh)) {
                            $data['kh_mbh'] = $kh_mbh;
                        }
                        //end
                        // Lấy thông tin gói
                        $data_get = array(
                            'id' => $goi_id,
                            'get_one' => TRUE,
                        );
                        $packages = $this->products_model->get_products_size_by_options($data_get);
                        if (is_object($packages)) {
                            $data['goi_bh'] = $packages;
                        }
                        //end
                    }
                }
            }
        }
        // Lấy toàn bộ khách hàng cá nhân được bh
        $ca_nhan_dbh = $this->customers_model->get_canhan_dbh();
        if(!empty($ca_nhan_dbh)){
//            echo '<pre>';
//            print_r($ca_nhan_dbh);
//            die;
            $data['ca_nhan_dbh'] = $ca_nhan_dbh;
        }
        // Lấy toàn bộ kh cá nhân 
//        $data_get = array(
//            'skip_join' => TRUE,
//            'phanloai_kh'=>CANHAN,
//        );
//        $ca_nhan = $this->customers_model->get_customers($data_get);
//        if(!empty($ca_nhan)){
//            $data['ca_nhan'] = $ca_nhan;
//        }       
        //end
        $data['scripts'] = $this->scripts();
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }
     private function scripts() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    </script>';
        return $scripts;
    }
    /**
     * @desc: Hien thi danh sach cac khach hang dang ky fondend
     * 
     * @param type $options 
     */
    function browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {
        $options = array('lang' => $para1, 'page' => $para2);
        $options = array_merge($options, $this->_get_data_from_filter());
        $this->phpsession->save('customers_lang', $options['lang']);
        $total_row = $this->customers_model->get_customers_count($options);
        $total_pages = (int) ($total_row / FAQ_ADMIN_POST_PER_PAGE);
        if ($total_pages * FAQ_ADMIN_POST_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;
        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * FAQ_ADMIN_POST_PER_PAGE;
        $options['limit'] = FAQ_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['customerss'] = $this->customers_model->get_customers($options);
        $options['post_uri'] = 'customers_admin';
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page_links'] = $this->pagination->create_ajax_links();

        if ($options['lang'] <> DEFAULT_LANGUAGE) {
            $options['uri'] = CUSTOM_ADMIN_BASE_URL . '/' . $options['lang'];
        } else {
            $options['uri'] = CUSTOM_ADMIN_BASE_URL;
        }

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        $this->_view_data['main_content'] = $this->load->view('admin/customers_list', $options, TRUE);
        $this->_view_data['title'] = 'Quản lý khách hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    /**
     * Lấy dữ liệu từ filter
     * @return string
     */
    private function _get_data_from_filter() {
        $options = array();

        if ($this->is_postback()) {
            $options['search'] = $this->db->escape_str($this->input->post('search', TRUE));
            $this->phpsession->save('customers_search_options', $options);
        } else {
            $temp_options = $this->phpsession->get('customers_search_options');
            if (is_array($temp_options)) {
                $options['search'] = $temp_options['search'];
            } else {
                $options['search'] = '';
            }
        }
//        $options['offset'] = $this->uri->segment(3);
        return $options;
    }

    private function _get_posted_customers_data() {

        $post_data = array(
//            'fullname' => my_trim($this->input->post('fullname', TRUE)),
//            'cmt' => my_trim($this->input->post('cmt', TRUE)),
//            'DOB' => my_trim($this->input->post('DOB', TRUE)),
//            'phone' => my_trim($this->input->post('phone', TRUE)),
//            'email' => my_trim($this->input->post('email', TRUE)),
//            'address' => my_trim($this->input->post('address', TRUE)),

            'phanloai_kh' => my_trim($this->input->post('phanloai_kh', TRUE)),
            'phanloai_bh' => my_trim($this->input->post('phanloai_bh', TRUE)),
            'hopdong_baohiem' => my_trim($this->input->post('hopdong_baohiem', TRUE)),
            'congty_canhan' => my_trim($this->input->post('congty_canhan', TRUE)),
            'parent_id' => my_trim($this->input->post('parent_id', TRUE)),
            'ngaysinh' => my_trim($this->input->post('ngaysinh', TRUE)),
            'congtycon_chinhanh' => my_trim($this->input->post('congtycon_chinhanh', TRUE)),
            'phongban' => my_trim($this->input->post('phongban', TRUE)),
            'diachi' => my_trim($this->input->post('diachi', TRUE)),
            'mst_cmt' => my_trim($this->input->post('mst_cmt', TRUE)),
            'ngaycap' => my_trim($this->input->post('ngaycap', TRUE)),
            'noicap' => my_trim($this->input->post('noicap', TRUE)),
            'email' => my_trim($this->input->post('email', TRUE)),
            'dienthoai' => my_trim($this->input->post('dienthoai', TRUE)),
            'didong' => my_trim($this->input->post('didong', TRUE)),
            'phanloai_kh1' => my_trim($this->input->post('phanloai_kh1', TRUE)),
            'gioitinh' => my_trim($this->input->post('gioitinh', TRUE)),
                //
//            'ndbh_phanloai_kh' => my_trim($this->input->post('ndbh_phanloai_kh', TRUE)),
//            'ndbh_hopdong_baohiem' => my_trim($this->input->post('ndbh_hopdong_baohiem', TRUE)),
//            'ndbh_congty_canhan' => my_trim($this->input->post('ndbh_congty_canhan', TRUE)),
//            'ndbh_congtycon_chinhanh' => my_trim($this->input->post('ndbh_congtycon_chinhanh', TRUE)),
//            'ndbh_phongban' => my_trim($this->input->post('ndbh_phongban', TRUE)),
//            'ndbh_diachi' => my_trim($this->input->post('ndbh_diachi', TRUE)),
//            'ndbh_mst_cmt' => my_trim($this->input->post('ndbh_mst_cmt', TRUE)),
//            'ndbh_ngaycap' => my_trim($this->input->post('ndbh_ngaycap', TRUE)),
//            'ndbh_noicap' => my_trim($this->input->post('ndbh_noicap', TRUE)),
//            'ndbh_email' => my_trim($this->input->post('ndbh_email', TRUE)),
//            'ndbh_dienthoai' => my_trim($this->input->post('ndbh_dienthoai', TRUE)),
//            'ndbh_didong' => my_trim($this->input->post('ndbh_didong', TRUE)),
//            'ndbh_phanloai_kh1' => my_trim($this->input->post('ndbh_phanloai_kh1', TRUE)),
//            'ndbh_quanhe' => my_trim($this->input->post('ndbh_quanhe', TRUE)),
        );

        if ($this->phpsession->get('roles_publisher') == STATUS_ACTIVE) {
            $post_data['status'] = STATUS_ACTIVE;
        } else {
            $post_data['status'] = STATUS_INACTIVE;
        }
        $created_date = datetimepicker_array2($this->input->post('created_date', TRUE));
        $post_data['created_date'] = date('Y-m-d H:i:s', mktime($created_date['hour'], $created_date['minute'], $created_date['second'], $created_date['month'], $created_date['day'], $created_date['year']));
        $post_data['updated_date'] = date('Y-m-d H:i:s');
        $post_data['editor'] = $this->phpsession->get('user_id');

        $ngaycap = datetimepicker_array2($this->input->post('ngaycap', TRUE));
        //$ndbh_ngaycap = datetimepicker_array2($this->input->post('ndbh_ngaycap', TRUE));
        $ngaysinh = datetimepicker_array2($this->input->post('ngaysinh', TRUE));
        $post_data['ngaycap'] = date('Y-m-d H:i:s', mktime($ngaycap['hour'], $ngaycap['minute'], $ngaycap['second'], $ngaycap['month'], $ngaycap['day'], $ngaycap['year']));
        //$post_data['ndbh_ngaycap'] = date('Y-m-d H:i:s', mktime($ndbh_ngaycap['hour'], $ndbh_ngaycap['minute'], $ndbh_ngaycap['second'], $ndbh_ngaycap['month'], $ndbh_ngaycap['day'], $ndbh_ngaycap['year']));
        $post_data['ngaysinh'] = date('Y-m-d H:i:s', mktime($ngaysinh['hour'], $ngaysinh['minute'], $ngaysinh['second'], $ngaysinh['month'], $ngaysinh['day'], $ngaysinh['year']));

        return $post_data;
    }

    /*
     * Thêm mới sản phẩm
     */

    function add() {
        $options = $data = array();

        //$options = $this->_get_posted_customers_data();

        if ($this->is_postback()) {
            if (!$this->_do_add_customers())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //$options += $this->_get_add_customers_form_data();
        // Chuan bi du lieu chinh de hien thi
        $options['scripts'] = $this->_get_scripts();
        $options['header'] = 'Thêm khách hàng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/add';

        // Get all customer bussiness phanloai_kh
        $customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP));
        if (!empty($customers_b)) {
            $options['customers_b'] = $customers_b;
        }
        //end
        $this->_view_data['main_content'] = $this->load->view('admin/add_customers_form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Thêm khách hàng' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    /**
     * Chuẩn bị dữ liệu cho form add
     * @return type
     */
    private function _get_add_customers_form_data() {
        $options = array();
        $options['fullname'] = my_trim($this->input->post('fullname'));
        $options['cmt'] = my_trim($this->input->post('cmt'));
        $options['phone'] = my_trim($this->input->post('phone'));
        $options['email'] = $this->input->post('email');
        $options['DOB'] = $this->input->post('DOB');
        $options['address'] = my_trim($this->input->post('address'));

        if ($this->is_postback()) {
            $created_date = datetimepicker_array2($this->input->post('created_date', TRUE));
            $options['created_date'] = date('d-m-Y H:i', mktime($created_date['hour'], $created_date['minute'], $created_date['second'], $created_date['month'], $created_date['day'], $created_date['year']));
            $options['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->input->post('lang', TRUE), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
        } else {
            $options['created_date'] = date('d-m-Y H:i');
            $options['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->phpsession->get('news_lang'), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
        }

        $options['scripts'] = $this->_get_scripts();
        $options['header'] = 'Thêm khách hàng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/add';

        return $options;
    }

    private function _do_add_customers($uri = 0) {
        $this->form_validation->set_rules('phanloai_kh', 'Phân loại khách hàng', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('phanloai_bh', 'Loại hình bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('hopdong_baohiem', 'Hợp', 'trim|required|xss_clean');
        $this->form_validation->set_rules('congty_canhan', 'Tên Công ty / Cá nhân', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ngaysinh', 'Ngày sinh', 'trim|xss_clean');
        $this->form_validation->set_rules('congtycon_chinhanh', 'Công ty con / Chi nhánh', 'trim|xss_clean|max_length[512]');
        $this->form_validation->set_rules('phongban', 'Phòng ban', 'trim|xss_clean');
        $this->form_validation->set_rules('diachi', 'Địa chỉ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mst_cmt', 'ĐKKD - MST / CMTND - Hộ Chiếu', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ngaycap', 'Ngày cấp', 'trim|required|xss_clean');
        $this->form_validation->set_rules('noicap', 'Nơi cấp', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dienthoai', 'Điện thoại', 'trim|required|xss_clean');
        $this->form_validation->set_rules('didong', 'Di động', 'trim|required|xss_clean');
        $this->form_validation->set_rules('phanloai_kh1', 'Phân loại khách hàng', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gioitinh', 'Giới tính', 'trim|required|xss_clean');
        $this->form_validation->set_rules('parent_id', 'Trực thuộc công ty', 'trim|required|xss_clean');
        //
//        $this->form_validation->set_rules('ndbh_phanloai_kh', 'Thông tin người được bảo hiểm - Loại khách hàng', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_hopdong_baohiem', 'Thông tin người được bảo hiểm - Hợp đồng bảo hiểm', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_congty_canhan', 'Thông tin người được bảo hiểm - Tên Công ty / Cá nhân', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_congtycon_chinhanh', 'Thông tin người được bảo hiểm - Công ty con / Chi nhánh', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_phongban', 'Thông tin người được bảo hiểm - Phòng ban', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_diachi', 'Thông tin người được bảo hiểm - Địa chỉ', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_mst_cmt', 'Thông tin người được bảo hiểm - ĐKKD - MST / CMTND - Hộ Chiếu', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_ngaycap', 'Thông tin người được bảo hiểm - Ngày cấp', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_noicap', 'Thông tin người được bảo hiểm - Nơi cấp', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_email', 'Thông tin người được bảo hiểm - Email', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_dienthoai', 'Thông tin người được bảo hiểm - Điện thoại', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_didong', 'Thông tin người được bảo hiểm - Di động', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_phanloai_kh1', 'Thông tin người được bảo hiểm - Phân loại khách hàng', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('ndbh_quanhe', 'Thông tin người được bảo hiểm - Quan hệ', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {

            $post_data = $this->_get_posted_customers_data();
            $post_data['creator'] = $this->phpsession->get('user_id');

            $post_data['id'] = $id = $this->input->post('id', TRUE);

            // Update data
            if ($id > 0) {
                $this->customers_model->update($post_data);
                redirect(CUSTOM_ADMIN_BASE_URL . '/' . $post_data['lang']);
            } else {
                // Creat data
                $insert_id = $this->customers_model->insert($post_data);
                if ($uri == 2) {
                    return TRUE;
                } else {
                    redirect(CUSTOM_ADMIN_BASE_URL . '/' . $post_data['lang']);
                }
            }
        }
        return FALSE;
    }

    function add_from_pro() {
        if ($this->is_postback()) {


            if ($this->_do_add_customers(2)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    function edit() {
        $options = array();

        if (!$this->is_postback())
            redirect(CUSTOM_ADMIN_BASE_URL);

        if ($this->is_postback() && !$this->input->post('from_list')) {

            if (!$this->_do_add_customers())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }

        $id = $this->input->post('id');
        // Get customers
        $customers = $this->customers_model->get_customers(array('id' => $id));
        if (is_object($customers)) {
            $options['customers'] = $customers;
        }
        //$options += $this->_get_edit_form_data();

        $options['header'] = 'Xem chi tiết khách hàng';
        $options['button_name'] = 'Sửa Khách hàng';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/edit';
        $options['scripts'] = $this->_get_scripts();

        // Get all customer bussiness phanloai_kh
        $customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP, 'skip_id' => $id));
        if (!empty($customers_b)) {
            $options['customers_b'] = $customers_b;
        }
        //end

        $this->_view_data['main_content'] = $this->load->view('admin/add_customers_form', $options, TRUE);
        $this->_view_data['title'] = 'Xem đơn hàng của khách' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    /**
     * Chuẩn bị dữ liệu cho form sửa
     * @return type
     */
    private function _get_edit_form_data() {
        $id = $this->input->post('id');
        // khi vừa vào trang sửa
        if ($this->input->post('from_list')) {
            $customers = $this->customers_model->get_customers(array('id' => $id));
            $id = $customers->id;
            $phone = $customers->phone;
            $DOB = $customers->DOB;
            $address = $customers->address;
            $fullname = $customers->fullname;
            $cmt = $customers->cmt;
            $email = $customers->email;
            $created_date = $customers->created_date;
        }
        // khi submit
        else {
            $id = $id;
            $phone = my_trim($this->input->post('phone', TRUE));
            $DOB = my_trim($this->input->post('DOB', TRUE));
            $address = my_trim($this->input->post('address', TRUE));
            $fullname = my_trim($this->input->post('fullname', TRUE));
            $cmt = my_trim($this->input->post('cmt', TRUE));
            $email = my_trim($this->input->post('email', TRUE));
            $created_date = my_trim($this->input->post('created_date', TRUE));
        }
        $options = array();
        $options['id'] = $id;
        $options['phone'] = $phone;
        $options['DOB'] = $DOB;
        $options['address'] = $address;
        $options['fullname'] = $fullname;
        $options['cmt'] = $cmt;
        $options['email'] = $email;
        $options['created_date'] = $created_date;
        $options['header'] = 'Xem chi tiết khách hàng';
        $options['button_name'] = 'Sửa Khách hàng';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/edit';
        $options['scripts'] = $this->_get_scripts();
        return $options;
    }

    /**
     *  sửa trong DB nếu Validate OK
     * @return type
     */
    private function _do_edit_customers() {
        $post_data = $this->_get_posted_customers_data();
        $post_data['id'] = $this->input->post('id');

        $this->customers_model->update($post_data);
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    /**
     * Xóa tin
     */
    public function delete() {
        $options = array();
        if ($this->is_postback()) {
            $id = $this->input->post('id');
            $this->customers_model->delete($id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    private function _get_scripts() {
        $scripts = '<script type="text/javascript" src="/plugins/tinymce/tinymce.min.js?v=4.1.7"></script>';
        $scripts .= '<link rel="stylesheet" type="text/css" href="/plugins/fancybox/source/jquery.fancybox.css" media="screen" />';
        $scripts .= '<script type="text/javascript" src="/plugins/fancybox/source/jquery.fancybox.pack.js"></script>';
        $scripts .= '<script type="text/javascript">$(".iframe-btn").fancybox({"width":900,"height":500,"type":"iframe","autoScale":false});</script>';
        $scripts .= '<style type=text/css>.fancybox-inner {height:500px !important;}</style>';
        $scripts .= '<script type="text/javascript">enable_tiny_mce();</script>';

        return $scripts;
    }

    function change_status() {
        $id = $this->input->post('id');
        $customers = $this->customers_model->get_customers(array('id' => $id));
        $status = $customers->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->customers_model->update(array('id' => $id, 'status' => $status));
    }

    public function up() {
        $customers_id = $this->input->post('id');
        $this->customers_model->update(array('id' => $customers_id, 'updated_time' => date('Y-m-d H:i:s')));
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    function export($options = array()) {
        $options = array();
//        if($this->phpsession->get('customer_name_search') != '')
//            $options['keyword'] = $this->phpsession->get('customer_name_search');
//        if(is_array($this->phpsession->get('date_filter'))){
//            $options = array_merge($options, $this->phpsession->get('date_filter'));
//        }
        $options = array_merge($options, $this->_get_data_from_filter());
        $customers = $this->customers_model->get_customers($options);

        if (count($customers) > 0) {

            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Order excel');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'MÃ KHÁCH HÀNG');
            $this->excel->getActiveSheet()->setCellValue('C1', 'HỌ VÀ TÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('E1', 'ĐIỆN THOẠI');
            $this->excel->getActiveSheet()->setCellValue('F1', 'ĐỊA CHỈ');

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $stt = 1;
            $row = 3;
            $total_m = 0;

            foreach ($customers as $order):

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $stt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $order->fullname);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->email);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->phone);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $order->address);

                $stt++;
                $row++;
            endforeach;
            $row++;
            $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('B' . $row . ':Z' . $row);

            $filename = 'dang_sach_khach_hang.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->list_customers(array('error' => 'Mục bạn đã chọn hiện thời không có khách hàng nào!'));
        }
    }

}
