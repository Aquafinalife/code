<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['news_letter']                           = 'emails/emails/add_email';
/*******************************************************************************/
// Phần backend-routing, không cần thay đổi cho các dự án của khách hàng
// chỉ thay đổi trong những trường hợp thực sự cần thiết
/*******************************************************************************/
// back end
//$route['^dashboard/emails']                     = 'emails/emails_admin/dispatcher/list_emails/1';
$route['^dashboard/emails/page-(\d+)$']         = 'emails/emails_admin/dispatcher/list_emails/$1';
$route['^dashboard/emails/download$']           = 'emails/emails_admin/download';
$route['^dashboard/emails/delete/(\d+)$']       = 'emails/emails_admin/delete/$1';

/*******************************************************************************/
// Kết thúc phần backend-routing
/*******************************************************************************/