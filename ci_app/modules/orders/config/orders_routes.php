<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['^dashboard/orders/btcn']            = 'orders/orders_admin/btcn';
$route['^dashboard/orders/btdn']            = 'orders/orders_admin/btdn';

$route['^dashboard/orders/thong_tin_bao_hiem']            = 'orders/orders_admin/thong_tin_bao_hiem';
$route['^dashboard/orders/thong_tin_boi_thuong']            = 'orders/orders_admin/thong_tin_boi_thuong';