<?php
$this->load->view('admin/orders_nav');
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', ORDER_ADMIN_BASE_URL);
echo form_close();
$key = isset($_GET['key']) ? $_GET['key'] : '';
?>
<div class="form_content">
    <?php // $this->load->view('admin/filter_form_bc_1'); ?>
    <div class="filter">
        <?php // echo form_open('dashboard/orders/btcn'); ?>
        <form method="get" action="<?php echo site_url('dashboard/orders/btdn') ?>">
            <!--Ngôn ngữ:--> <?php // if (isset($lang_combobox)) echo $lang_combobox;     ?>
            Tên doanh nghiệp mua bảo hiểm: 
            <select name="key" class="js-example-disabled-results">
                <option value="">Điền Tên Doanh Nghiệp Mua BH</option>            
                <?php
                if (isset($a_dn_bbh) && !empty($a_dn_bbh)) {
                    $a_dn = array();
                    foreach ($a_dn_bbh as $index) {
                        $name = $index->name;
                        $parent_id = $index->parent_id;
                        $id = $index->id;
                        $selected = ($id == $dn_bbh) ? 'selected="selected"' : '';
                        if ($parent_id == FALSE) {
                            ?>
                            <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                            <?php
                        }
                        $a_dn[] = $congty_canhan;
                    }
                }
                ?>
            </select>
            <input type="submit" name="submit" value="Tìm kiếm" class="btn" />
            <!--<span class="fright"><a class="button" href="/dashboard/orders/export"><em>&nbsp;</em>Xuất excel</a></span>-->
            <?php // echo form_close(); ?>
        </form>
    </div>

    <?php $this->load->view('powercms/message'); ?>
    <?php if (!empty($cmt) && $custom != '') { ?>
        <h3>1.Thông tin người được bảo hiểm</h3>
        <style>
            .list{
                clear: none;
            }
        </style>
        <div style="width: 100%">
            <table class="list" style="width: 50%; margin-bottom: 10px; float: left;">
                <thead>
                    <tr>
                        <th style="width: 30%">Option</th>
                        <th>Value</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>MÃ KH</strong></td>
                        <td><?php echo $custom->id ?></td>

                    </tr>
                    <tr>
                        <td><strong>KHÁCH HÀNG</strong></td>
                        <td><?php echo $custom->fullname ?></td>

                    </tr>
                    <tr>
                        <td><strong>CMT</strong></td>
                        <td><?php echo $custom->cmt ?></td>

                    </tr>
                    <tr>
                        <td><strong>HỢP ĐỒNG BH</strong></td>
                        <td><?php echo $custom->so_hop_dong ?></td>

                    </tr>					
                    <tr>
                        <td><strong>CTY</strong></td>
                        <td><?php echo $custom->congty_canhan ?></td>

                    </tr>
                    <tr>
                        <td><strong>ĐỊA CHỈ</strong></td>
                        <td><?php echo $custom->diachi ?></td>

                    </tr>
                    <tr>
                        <td><strong>ĐKKD-Mst/CMT-HỘ CHIẾU</strong></td>
                        <td><?php echo $custom->mst_cmt ?></td>

                    </tr>

                </tbody>
            </table>
            <table class="list" style="width: 50%; margin-bottom: 10px;float:left;">
                <thead>
                    <tr>
                        <th style="width: 30%">Option</th>
                        <th>Value</th>

                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td><strong>NGÀY CẤP</strong></td>
                        <td><?php echo $custom->ngaycap ?></td>

                    </tr>
                    <tr>
                        <td><strong>NƠI CẤP</strong></td>
                        <td><?php echo $custom->noicap ?></td>

                    </tr>
                    <tr>
                        <td><strong>EMAIL</strong></td>
                        <td><?php echo $custom->email ?></td>

                    </tr>
                    <tr>
                        <td><strong>PHONE</strong></td>
                        <td><?php echo $custom->phone ?></td>

                    </tr>
                    <tr>
                        <td><strong>MOBILE</strong></td>
                        <td><?php echo $custom->phone2 ?></td>

                    </tr>
                    <tr>
                        <td><strong>PHÂN LOẠI KH</strong></td>
                        <td><?php echo $custom->type_custom ?></td>

                    </tr>
                    <tr>
                        <td><strong>Quan hệ</strong></td>
                        <td><?php echo $custom->ndbh_quanhe ?></td>

                    </tr>

                </tbody>
            </table>
        </div>
        <br class="clear"/>&nbsp;
        <h3>2.Thông tin bồi thường</h3>
        <table class="list" style="width: 100%; margin-bottom: 10px;">
            <thead>
                <tr>
                    <th style="width: 30%">Option</th>
                    <th>Default</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>TỔNG SỐ HSBT</strong></td>
                    <td><?php echo $total_orders ?></td>

                </tr>
                <tr>
                    <td><strong>TỔNG SỐ TIỀN YÊU CẦU BT</strong></td>
                    <td><?php echo $total_ycbt->total ?></td>

                </tr>
                <tr>
                    <td><strong>TỔNG SỐ TIỀN BT</strong></td>
                    <td><?php echo $price_cnbt->price_cnbt ?></td>

                </tr>
                <tr>
                    <td><strong>TỶ LỆ BT HIỆN TẠI</strong></td>
                    <td><?php ?></td>

                </tr>	

            </tbody>
        </table>
        <br class="clear"/>&nbsp;
    <?php } elseif ($custom == '') { ?>
        <p class="bg-primary">Không tìm thấy khách hàng này vui lòng nhập chính xác chứng minh thư</p>
    <?php } else { ?>
        <p class="bg-primary">Xin mời nhập chứng minh thư.</p>
    <?php } ?>
</div>
