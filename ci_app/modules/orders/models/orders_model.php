<?php
class Orders_Model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function get_orders($options = array())
    {
   
        $this->db->select(' orders.*,
                        
                    ');

//        $this->db->join('orders_categories', 'orders_categories.id = orders.cat_id', 'left');

        // lọc ngôn ngữ
        if(!empty($options['lang']))
        {
            $this->db->where('orders.lang', $options['lang']);
        }
        
        //loc don hang nao cua user nao
        if(isset($options['user_id']))
        {
            $this->db->where('orders.user_id', $options['user_id']);
        }
        
        //loc don hang nao cua user nao
        if(isset($options['cmt']))
        {
            $this->db->where('orders.cmt_custom_duoc', $options['cmt']);
        }
        
        //loc theo id don hang 
        if(isset($options['id']) && $options['id'] !='')
        {
            $this->db->where('orders.id', $options['id']);
        }
        
        if(isset($options['start_date_m'])  ){
          
            $this->db->where('orders.update_time >=', $options['start_date_m']);
        }
        if( isset($options['end_date_m']) ){
            $this->db->where('orders.update_time <=', $options['end_date_m']);
           
        }
        
        if(isset($options['status']))
        {
            $this->db->where('orders.status', $options['status']);
        }
        
        if(!empty($options['order_status']) && $options['order_status']!= '-1')
        {
            $this->db->where('orders.order_status', $options['order_status']);
        }
        
        if(isset($options['fullname']))
        {
            $this->db->where('orders.fullname', $options['fullname']);
        }
        
        if (!empty($options['search']))
            $this->db->like('orders.fullname', $options['search']);
        
        if(!empty($options['email']))
        {
            $this->db->where('orders.email', $options['email']);
        }

     
        // Loc theo trang
        if (isset($options['limit']) && isset($options['offset']))
            $this->db->limit($options['limit'], $options['offset']);
        else if (isset($options['limit']))
            $this->db->limit($options['limit']);

        if(isset($options['sort_by_viewed'])){
            $this->db->order_by('orders.viewed desc, orders.updated_date desc, orders.created_date desc');
        }elseif(isset($options['random']) && $options['random'] == TRUE){
            $this->db->order_by('RAND()');
        }else{
            $this->db->order_by('orders.update_time desc, orders.create_time desc');
        }
       
        $query = $this->db->get('orders');
       
        if($query->num_rows() > 0){
            if (isset($options['one_cod'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if(isset($options['onehit']) && $options['onehit'] == TRUE){
                return $query->row(0);
            }
            return $query->result();
        }else{
            return NULL;
        }
        
    }

    public function get_orders_count($options = array())
    {
        return count($this->get_orders($options));
    }

    public function update_orders_view($id = 0)
    {
        $this->db->set('viewed', 'viewed + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('orders');
    }
    
    public function update_orders_cl($options = array())
    {
        $this->db->set('stbh_cl', $options['stbh_cl'], FALSE);
        $this->db->set('st_dbt', $options['st_dbt'], FALSE);
        $this->db->where('cmt_custom', $options['cmt_custom']);
        $this->db->where('loai_benh', $options['loai_benh']);
        $this->db->update('itnl_clbh');
    }
    
    public function position_to_add_orders($options=array())
    {
        if (!isset($options['lang'])) {
            $options['lang'] = DEFAULT_LANGUAGE;
        }
        $news = $this->db
                ->select('id,position')
                ->where('lang',  $options['lang'])
                ->order_by('position desc')
                ->limit(1)
                ->get('news');
        if($news->num_rows() > 0){
            $news = $news->row(0);
            $position_add = $news->position + 1;
        }else{
            $position_add = 1;
        }
        return $position_add;
    }
    
    public function insert_cl($data)
    {

        $this->db->insert('itnl_clbh', $data);
        return $this->db->insert_id();
    }
    
    public function sum_custom($options = array())
    {
        
        if(isset($options['cmt']))
        {
            $this->db->where('orders.cmt_custom_duoc', $options['cmt']);
        }
        if(!empty($options['total']))
        {
            $this->db->select_sum('total');
//            $this->db->select('total');
        }
        if(!empty($options['price_cnbt']))
        {
            $this->db->select_sum('price_cnbt');
//            $this->db->select('total');
        }
        
        $query = $this->db->get('orders');
       
        if($query->num_rows() > 0){
            if (isset($options['one_cod'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if(isset($options['onehit']) && $options['onehit'] == TRUE){
                return $query->row(0);
            }
            return $query->result();
        }else{
            return NULL;
        }
    }
    
}
?>